PROGRAM getLine
	IMPLICIT NONE
!Given a list of line numbers print out lines in second file
	INTEGER :: i,j,k,lineCount,lineCount2,aerr

	INTEGER,ALLOCATABLE,DIMENSION(:) :: line
	CHARACTER(len=1000), ALLOCATABLE,DIMENSION(:) :: dataLine
	DOUBLE PRECISION :: tmp

	LOGICAL :: fileExists
      CHARACTER(len=100) :: fin,fin2
      CHARACTER(len=1000) :: BUFFER

	!file with line numbers
	call GETARG(1,fin)
	!file to extract from
	call GETARG(2,fin2)

	fileExists=.false.
 	inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
      DO WHILE (.true.)
		read(1, '(A)', end=99) BUFFER
		lineCount=lineCount+1
  	ENDDO
99  	CONTINUE
    	CLOSE(1)

	fileExists=.false.
 	inquire(FILE=fin2(1:LEN_TRIM(fin2)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if
	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')

	lineCount2=0
      DO WHILE (.true.)
		read(1, '(A)', end=98) BUFFER
		lineCount2=lineCount2+1
  	ENDDO
98  	CONTINUE
    	CLOSE(1)

    	aerr=0

	ALLOCATE(line(1:lineCount),dataLine(1:lineCount2),STAT=aerr)
	 IF (aerr > 0) THEN
            WRITE(0,*)
            WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(0,*)
            STOP 'Abnormal program termination!!!'
         END IF


    	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
    	READ(1,*) (line(i), i=1,lineCount)
	CLOSE(1)
	
    	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
    	READ(1,'(A)') (dataLine(i), i=1,lineCount2)
	CLOSE(1)

	DO i=1,lineCount
		write(*,*) trim(dataLine(line(i)))
	END DO

END PROGRAM