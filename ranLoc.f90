PROGRAM ranLoc

	! Assign a location in 3D space to a set of stars that were 
    ! randomly created by "population.f90" and written to a "ranPick.dat" file

	
	USE randomNum
	USE integration
	USE distributions
	USE polygon
	USE extinction
	USE input
	USE params

	IMPLICIT NONE

	INTEGER :: i, j, l, ii, jj, k, kk, nn, a, b, c, j1, k1, l1
	INTEGER :: aerr, jmax
	INTEGER :: lineCount, lineCount2, clock, curMax
	INTEGER, PARAMETER :: itmax = 200, ndist = 5000

    ! INTEGER, PARAMETER :: nlon=25, nlat=nlon, nlbsub = nlon+1

	INTEGER, PARAMETER :: nlbsub=26
	INTEGER,  ALLOCATABLE,  DIMENSION(:) :: ranStar
	INTEGER :: i1, i2
	INTEGER :: minID, maxID, midID, res, res2

	DOUBLE PRECISION :: tmp, pi, ranNum, ranA, ranB, ranC
	DOUBLE PRECISION :: dlon, dlat, deltad, bran, lran, dran
	DOUBLE PRECISION :: lmid, bmid, appMag, ext

    ! REAL :: rdmin, rdmax, rdmx, rdmx1, rdmx2
    ! REAL :: avt, savt, avc, avg
    ! REAL :: l0deg, b0deg

 	DOUBLE PRECISION,  DIMENSION(1:nlbsub) :: l0, b0, l0deg, b0deg


	DOUBLE PRECISION,  PARAMETER :: eps = 1.d-2, dmaxd=50000.0
	DOUBLE PRECISION,  DIMENSION(1:nlbsub) :: lon, lat
	DOUBLE PRECISION,  ALLOCATABLE,  DIMENSION(:) ::ifgCul, magUsed, dmin, dmax, dist
	DOUBLE PRECISION,  ALLOCATABLE,  DIMENSION(:, :) :: mag
	DOUBLE PRECISION,  ALLOCATABLE,  DIMENSION(:, :, :) :: ifg

	! Input variables

    ! INTEGER :: class, nchn0, nt, wantTransit, isSingle, numTransits
    ! INTEGER, DIMENSION(:), ALLOCATABLE :: chn0
    ! CHARACTER(len=10) :: model
    ! CHARACTER(len=5) :: sfm, age
    ! DOUBLE PRECISION :: lon1, lon2, lat1, lat2
    ! DOUBLE PRECISION :: R0, z0, hR, hz
    ! DOUBLE PRECISION :: ta, tb, Tg, T0
    ! DOUBLE PRECISION :: minMag, maxMag

    ! Extinction color correction Av=Ar/colourCorrect
    ! DOUBLE PRECISION, PARAMETER :: colourCorrect=0.88

	LOGICAL :: fileExists
	CHARACTER(len=250) :: fin1, fin2, fin3, popField, pop_file
	CHARACTER(len=1000) :: BUFFER

    ! CHARACTER(len=1) :: extType
    ! 
    ! 
    ! DOUBLE PRECISION :: avt, savt, avc, avg
    ! DOUBLE PRECISION, DIMENSION(5) :: av, sav

	aerr = 0

	! Define pi
	pi   = ACOS(-1.d0)

	! WEB_ran
	call get_command_argument(1, fin1)

	! WEB_mag
	call get_command_argument(2, fin2)

	! pop.in
	call get_command_argument(3, pop_file)

    ! call get_command_argument(4, popField)



    ! get input parameters (from 'pop.in' file)
	CALL read_pop_params(pop_file)



	fileExists=.false.
	inquire(FILE=fin1(1:LEN_TRIM(fin1)), exist=fileExists)
	if(fileExists .eqv. .false.) then
		write(0, *) "No file 1",  fin1
		stop
	end if
	OPEN(1, FILE=fin1, STATUS='OLD', ACTION='READ')

	lineCount=0
	DO WHILE (.true.)
		read(1,  '(A)',  end=99) BUFFER
		lineCount=lineCount+1
	ENDDO
99  	CONTINUE
	CLOSE(1)
	aerr=0
	
	
	
	fileExists=.false.
	inquire(FILE=fin2(1:LEN_TRIM(fin2)), exist=fileExists)
	if(fileExists .eqv. .false.) then
		write(0, *) "No file 2",  fin2
		stop
	end if
	OPEN(1, FILE=fin2, STATUS='OLD', ACTION='READ')

	lineCount2=0
	DO WHILE (.true.)
		read(1,  '(A)',  end=98) BUFFER
		lineCount2=lineCount2+1
	ENDDO
98  	CONTINUE
	CLOSE(1)
	aerr=0



	ALLOCATE(ranStar(1:lineCount), &
             mag(1:3, 1:lineCount2), &
             magUsed(1:lineCount), &
             dmin(1:lineCount), &
             dmax(1:lineCount), &
             STAT=aerr)

	IF (aerr > 0) THEN
		WRITE(0, *)
		WRITE(0, *) '1,  ERROR ALLOCATING MEMORY - ranStar, mag, MagUsed etc!'
		WRITE(0, *)
		STOP 'Abnormal program termination!!!'
	END IF

    ! write(*, *) "1"

    ! ef: just reading one record here?
	OPEN(1, FILE=fin1, STATUS='OLD', ACTION='READ')
	READ(1, *) (ranStar(i),  i=1, lineCount)
	CLOSE(1)

    ! ef: just reading one record here?
	OPEN(1, FILE=fin2, STATUS='OLD', ACTION='READ')
	READ(1, *) (tmp, mag(1, i), mag(2, i), mag(3, i),  i=1, lineCount2)
	CLOSE(1)

    ! CALL readPop(fin3)
	nlon = nlbsub - 1
    nlat = nlon

    ! write(*, *) "2"

    ! Setup 2D grid (bins) of 
    ! galactic longitudes and latitudes in radians

    ! ALLOCATE(lon(nlon+1), lat(nlat+1),  STAT=aerr)
    ! IF (aerr > 0) THEN
    ! WRITE(0, *)
    ! WRITE(0, *) '85,  ERROR ALLOCATING MEMORY!'
    ! WRITE(0, *)
    ! call setError()
    ! STOP 'Abnormal program termination!!!'
    ! END IF


    ! convert to radians
	lon1 = lon1 * pi/180.d0
	lon2 = lon2 * pi/180.d0
	lat1 = lat1 * pi/180.d0
	lat2 = lat2 * pi/180.d0

	dlon = (lon2 - lon1) / DBLE(nlon)
	dlat = (lat2 - lat1) / DBLE(nlat)

	CALL create_bins(lon1, dlon, nlon, bin_edges=lon)
	CALL create_bins(lat1, dlat, nlat, bin_edges=lat)

    ! write(0, *) "3"
    ! write(*, *) lon1, lon2
    ! write(*, *) lon

    ! Integrate Galactic potential 
    ! and store for later use

	ALLOCATE(dist(ndist+1),  &
             ifg(nlbsub, nlbsub, ndist+1), &
             ifgCul((nlbsub+1) * (nlbsub+1) * (ndist+1)),  &
             STAT=aerr)

	IF (aerr > 0) THEN
        WRITE(0, *)
        WRITE(0, *) '85,  ERROR ALLOCATING MEMORY - dist, ifg, ifgcul!'
        WRITE(0, *)
        STOP 'Abnormal program termination!!!'
	END IF

	deltad = dmaxd / DBLE(ndist)

	! Stellar density at each point in space
	CALL create_bins(0.d0, deltad, ndist, bin_edges=dist(1:ndist+1))

	DO ii=1, nlon
		do jj=1, nlat
			lmid = (lon(ii) + lon(ii+1)) / 2.d0
 			bmid = (lat(jj) + lat(jj+1)) / 2.d0

			ifg(ii, jj, 1) = 0.d0

			DO kk=2, ndist+1
				ifg(ii, jj, kk) = galDisc(dist(kk), lmid, bmid, R0, z0, hR, hz, dlon, dlat)
			enddo

		enddo
	enddo

    !  	write(0, *) "4"
    ! if(isSingle ==1)THEN
    ! FORALL(i=1:lineCount) magUsed(i)=mag(1, ranStar(i))
    ! ELSE
            FORALL(i=1:lineCount) magUsed(i) = mag(3, ranStar(i))
    ! END IF
    !  	write(0, *) "5"
    ! write(6, *) magUsed
    ! stop


    ! ALLOCATE(l0(1:nlon+1), b0(1:nlon+1), &
    ! l0deg(1:nlon+1), b0deg(1:nlon+1),  STAT=aerr)
    ! IF (aerr > 0) THEN
    ! WRITE(0, *)
    ! WRITE(0, *) ' ERROR ALLOCATING MEMORY!'
    ! WRITE(0, *)
    ! STOP 'Abnormal program termination!!!'
    ! END IF


	DO nn = 1,  nlbsub
		l0(nn) = lon(1) + DBLE(nn-1) * dlon
		b0(nn) = lat(1) + DBLE(nn-1) * dlat
	END DO

 	l0deg = l0 * 180.d0/pi


    ! write(0, *) "6"
    ! write(*, *) l0
    ! stop

	b0deg = b0 * 180.d0/pi
	WHERE(l0deg < 0.d0) l0deg = l0deg + 360.0

    ! write(*, *) "5a"

	! Overwrite the distances for each object
	dmin = 10.d0**((minMag - magUsed + 5.d0) / 5.d0)
	dmax = 10.d0**((maxMag - magUsed + 5.d0) / 5.d0)

	! Lets scale the system down if we go to far
	where (dmin .gt. dmaxd) dmin = dmaxd
	where (dmax .gt. dmaxd) dmax = dmaxd

    ! write(*, *)dmin(1), dmax(1)


	! Get distance corrected for extinction
	DO k=1, lineCount
		dmax(k) = distanceCalc(dmax(k), l0deg, b0deg, nlbsub, colourCorrect, dlon, dlat, extType)
		dmin(k) = distanceCalc(dmin(k), l0deg, b0deg, nlbsub, colourCorrect, dlon, dlat, extType)
	end do

	where (dmin .gt. dmaxd) dmin = dmaxd
	where (dmax .gt. dmaxd) dmax = dmaxd

    ! write(*, *) dmin(1), dmax(1), dist(1), dist(2)

	! Initialize random number generator
	CALL preIntRan()


    ! write(0, *) "7"
    ! write(0, *) "start picking"

	DO i=1, lineCount
		ifgCul = 0.d0
		curMax = 1

		CALL locate(dist(1:ndist+1), ndist+1, dmax(i), i2)
		CALL locate(dist(1:ndist+1), ndist+1, dmin(i), i1)

        ! write(*, *) "8", i1, i2

		! Assume we can see star to same 
        ! distance over all lat lon bins
		DO j=1, nlon

			DO k=1, nlat
				ifgCul(curMax:curMax + i2 - i1) = ifg(j, k, i1:i2)
				curMax = curMax + i2 - i1 + 1
			END DO

		END DO

        ! write(*, *) "9"
        ! write(*, *) dmin(i), i1, dist(i1), dmax(i), i2, dist(i2)
        ! stop

		DO j=2, curMax
			ifgCul(j) = ifgCul(j) + ifgCul(j-1)
		END DO

		ifgCul = ifgCul / MAXVAL(ifgCul)

        ! write(*, *) "10"

		DO
            ! Generate random number 
            ! in range 0 to 1
			ranNum = unif(0.d0, 1.d0)

			! Search culSum for where 'ran' 
            ! is within element i and i-1
			minID = 1
			maxID = curMax

			binarySearch : DO
				midID = floor(dble(maxID + minID) / 2)

                ! write(6, *) ifgCul(midID-1), ran, ifgCul(midID)

				IF(ranNum .ge. ifgCul(midID-1) .and. &
                   ranNum .lt. ifgCul(midID)) THEN
                    ! found it
					res = midID
					EXIT binarySearch

				ELSE IF(ranNum .lt. ifgCul(midID-1)) THEN
					maxID = midID

				ELSE
					minID = midID
				END IF
	
			END DO binarySearch

            ! write(*, *) ran, res, ifgCul(res)

			res2 = 1
			j1   = 1
            k1   = 1
            l1   = 1


			! Turn the bin number found in 'res' into 
            ! a bin in lat, lon and distance
			lon_loop: do j=1, nlon

				lat_loop: do k=1, nlat

                        do l=1, (i2 - i1 + 1)
                            if(res2 .eq. res) then
                                j1 = j
                                k1 = k
                                l1 = l
                                exit lon_loop
                            end if

                            res2 = res2 + 1
                        end do

				end do lat_loop

			end do lon_loop


            ! write(*, *) j, k, l, res2

			! Perturb number lineraly from edge of bins
			lran   = unif(lon(j1), lon(j1) + dlon)
			bran   = unif(lat(k1), lat(k1) + dlat)
 			dran   = dist(l+i1) + ranC * deltad
 			
			ext    = extinct(lran*180.d0/pi, bran*180.d0/pi, dran/1000.0, extType, colourCorrect)
			appMag = 5.0 * log10(dran) + magUsed(i) - 5.0 + ext

			! if (point_in_poly(lran, bran) .and. (appMag .ge. minMag) .and. (appMag .le. maxMag)) exit
			if ((appMag .ge. minMag) .and. (appMag .le. maxMag)) exit
		END DO

        ! write(*, *) "*"
		
        lran = lran * 180.d0/pi
        bran = bran * 180.d0/pi

        ! write to "ranloc.dat" file
  		write(6, *) ranStar(i), magUsed(i), lran, bran, dran, ext, appMag
	END DO


END PROGRAM ranLoc

