PROGRAM channelProcess
!Take the WEB_pop_dat.1 file and matchs agaisnt the output of population so
!that we can see the numbers and types of systems
IMPLICIT NONE
	INTEGER :: i,j,k,lineCount,lineCount2,lineCount3,aerr

	INTEGER,ALLOCATABLE,DIMENSION(:) :: chan,typeNum
	DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:) :: num,typeSum
	CHARACTER(len=100), ALLOCATABLE,DIMENSION(:) :: typeChar
	DOUBLE PRECISION :: tmp

	LOGICAL :: fileExists
      CHARACTER(len=100) :: fin,fin2,fin3
      CHARACTER(len=1000) :: BUFFER

	!WEB_pop_dat.1
	call GETARG(1,fin)
	!WEB_{popCount,weight,merge etc}
	call GETARG(2,fin2)
	!WEB.chd.1
	call GETARG(3,fin3)

	fileExists=.false.
 	inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
      DO WHILE (.true.)
		read(1, '(A)', end=99) BUFFER
		lineCount=lineCount+1
  	ENDDO
99  	CONTINUE
    	CLOSE(1)

	fileExists=.false.
 	inquire(FILE=fin2(1:LEN_TRIM(fin2)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if
	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')

	lineCount2=0
      DO WHILE (.true.)
		read(1, '(A)', end=98) BUFFER
		lineCount2=lineCount2+1
  	ENDDO
98  	CONTINUE
    	CLOSE(1)

    	fileExists=.false.
 	inquire(FILE=fin3(1:LEN_TRIM(fin3)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin3
 		stop
 	end if

 	OPEN(1,FILE=fin3,STATUS='OLD',ACTION='READ')

	lineCount3=0
      DO WHILE (.true.)
		read(1, '(A)', end=97) BUFFER
		lineCount3=lineCount3+1
  	ENDDO
97  	CONTINUE
    	CLOSE(1)

	if(lineCount .ne. lineCount2) THEN
		write(0,*) "Files not same size"
		write(0,*) lineCount,lineCount2
		write(0,*) fin
		write(0,*) fin2
	STOP
    	end if
    	aerr=0


	ALLOCATE(chan(1:lineCount),num(1:lineCount2),typeNum(1:lineCount3),&
	typeChar(1:lineCount3),typeSum(1:lineCount3),STAT=aerr)
	 IF (aerr > 0) THEN
            WRITE(0,*)
            WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(0,*)
            STOP 'Abnormal program termination!!!'
         END IF


    	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
    	READ(1,*) (tmp,tmp,tmp,tmp,tmp,tmp,chan(i), i=1,lineCount)
	CLOSE(1)
	
    	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
    	READ(1,*) (tmp,num(i), i=1,lineCount2)
	CLOSE(1)

    	OPEN(1,FILE=fin3,STATUS='OLD',ACTION='READ')
    	READ(1,*) (typeNum(i),typeChar(i), i=1,lineCount3)
	CLOSE(1)

	typeSum=0.d0
	DO i=1,lineCount
		typeSum(chan(i))=typeSum(chan(i))+num(i)
	END DO

	DO i=1,lineCount3
		write(*,*) typeNum(i),typeSum(i),trim(typeChar(i))
	END DO

END PROGRAM channelProcess