import numpy as np
import getopt
import sys as sys

opts, extra = getopt.getopt(sys.argv[1:],'')

#Arguments are WEB_ biseps_ numFolders
	 
fileBase=extra[0]
folderBase=extra[1]
numFolders=extra[2]

counter=0

#Blank the file
open(fileBase, 'w').close()
for i in range(1,int(numFolders)+1):
	fname=folderBase+"/"+str(i)+"/"+fileBase

	data=np.genfromtxt(fname,dtype=None,defaultfmt='f%i',converters = {0: lambda s: float(s or 0)})
		
	data['f0']=data['f0']+counter
	
	f_handle = file(fileBase, 'a')
	np.savetxt(f_handle,data,fmt="%s")
	counter=data['f0'][-1]
	f_handle.close()

	
	print fname

	