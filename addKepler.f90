PROGRAM addKepler
IMPLICIT NONE
INTEGER :: i,j,lineCount,aerr

TYPE :: dataType
	DOUBLE PRECISION :: num,dataIn
END TYPE dataType

TYPE(dataType), ALLOCATABLE, DIMENSION (:,:) :: input


LOGICAL :: fileExists
 CHARACTER(len=20) :: fileType
 CHARACTER(len=100) :: fin
 CHARACTER(len=1000) :: BUFFER

	call GETARG(1,fileType)
	fileType=adjustl(fileType)
	
	fin="10.180845_70.785336/"//fileType(1:LEN_TRIM(fileType))
	
	fileExists=.false.
 	inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file 1", fileType
 		stop
 	end if
	OPEN(1,FILE="10.180845_70.785336/"//fileType,STATUS='OLD',ACTION='READ')

	lineCount=0
    DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  			lineCount=lineCount+1
    ENDDO
99  CONTINUE
    CLOSE(1)
    
    ALLOCATE(input(1:lineCount,1:21),STAT=aerr)
         IF (aerr > 0) THEN
            WRITE(*,*)
            WRITE(*,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(*,*)
            STOP 'Abnormal program termination!!!'
         END IF
         
         
     CALL readFile("10.180845_70.785336/"//fileType,lineCount,input(:,1))     
     CALL readFile("10.453423_81.233205/"//fileType,lineCount,input(:,13))
     CALL readFile("11.114621_74.793998/"//fileType,lineCount,input(:,6))
     CALL readFile("11.946842_78.743328/"//fileType,lineCount,input(:,12))     
     CALL readFile("12.577065_72.320850/"//fileType,lineCount,input(:,5))
     CALL readFile("12.811651_82.738296/"//fileType,lineCount,input(:,18))
     CALL readFile("13.482423_76.384104/"//fileType,lineCount,input(:,11))     
     CALL readFile("14.026588_69.851270/"//fileType,lineCount,input(:,4))
     CALL readFile("14.338365_80.302498/"//fileType,lineCount,input(:,17)) 
     CALL readFile("14.969572_73.865859/"//fileType,lineCount,input(:,10))          
     CALL readFile("16.018822_77.812246/"//fileType,lineCount,input(:,16))
     CALL readFile("16.474953_71.324458/"//fileType,lineCount,input(:,9))
     CALL readFile("16.722099_81.979707/"//fileType,lineCount,input(:,21))
     CALL readFile("17.399710_75.382602/"//fileType,lineCount,input(:,15))
     CALL readFile("18.320462_79.512025/"//fileType,lineCount,input(:,20))     
     CALL readFile("19.038024_73.131625/"//fileType,lineCount,input(:,14))     
     CALL readFile("19.819649_76.931255/"//fileType,lineCount,input(:,19))     
     CALL readFile("7.1297658_75.706121/"//fileType,lineCount,input(:,3))     
     CALL readFile("8.0285841_79.628046/"//fileType,lineCount,input(:,8))
     CALL readFile("8.7399100_73.196899/"//fileType,lineCount,input(:,2))     
     CALL readFile("9.5531612_77.321568/"//fileType,lineCount,input(:,7))
   
     
     
     DO i=1,lineCount
    	 write(6,100) input(i,1)%num,(input(i,j)%dataIn,j=1,21)
     END DO
     
100 FORMAT (F12.4,21(E12.5,1X))
      
END PROGRAM addKepler

SUBROUTINE readFile(fileName,lineCount,input)
IMPLICIT NONE
INTEGER,INTENT(IN) :: lineCount
 CHARACTER(*),INTENT(IN) :: fileName
LOGICAL :: fileExists
INTEGER :: i
 
TYPE :: dataType
	DOUBLE PRECISION :: num,dataIn
END TYPE dataType

TYPE(dataType),DIMENSION(lineCount),INTENT(INOUT) :: input


	fileExists=.false.
 	inquire(FILE=fileName(1:LEN_TRIM(fileName)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file 2 ", fileName
 		stop
 	end if
	OPEN(1,FILE=fileName(1:LEN_TRIM(fileName)),STATUS='OLD',ACTION='READ')
	
	READ(1,*) (input(i)%num,input(i)%dataIn, i=1,lineCount)

	CLOSE(1)

END SUBROUTINE
