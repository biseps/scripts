#!/bin/bash
#$ -N popProcess
#$ -P physics
#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=4G
#$ -S /bin/bash
#$ -t 1-84:1
#$ -j y
#$ -o /padata/beta/users/rfarmer/data/keplerAll/cluster2

id=$1

script="/padata/beta/users/rfarmer/code/scripts"
source="/padata/beta/users/rfarmer/code/source"

base="/padata/beta/users/rfarmer/data/keplerAll"
version="0.5a"
type="WEB"

young="$base/pop_b_y/$SGE_TASK_ID"
old="$base/pop_b_o/$SGE_TASK_ID"
merge="$base/merge/b/$SGE_TASK_ID"

# for version in $(seq 1 12)
# do

cd $young
echo $young $version
$script/ranPick "$type"_popCount 0.5 > "$type"_ran.$version
$script/ranLoc "$type"_ran.$version ../../channels/"$type"_mag.dat.1 pop.in > "$type"_ranLoc.$version

cd $old
echo $old $version
$script/ranPick "$type"_popCount 0.5 > "$type"_ran.$version
$script/ranLoc "$type"_ran.$version ../../channels/"$type"_mag.dat.1 pop.in > "$type"_ranLoc.$version

echo "cat files"
cat "$young"/"$type"_ranLoc.$version "$old"/"$type"_ranLoc.$version > $merge/"$type"_ranLoc.$version




base="/padata/beta/users/rfarmer/data/kepler30"
type="WEBwide"

young="$base/pop_s_y/$SGE_TASK_ID"
old="$base/pop_s_o/$SGE_TASK_ID"
merge="$base/merge/s/$SGE_TASK_ID"

# for version in $(seq 1 12)
# do

cd $young
echo $young $version
$script/ranPick "$type"_popCount 0.5 > "$type"_ran.$version
$script/ranLoc "$type"_ran.$version ../../channels/"$type"_mag.dat.1 pop.in > "$type"_ranLoc.$version

cd $old
echo $old $version
$script/ranPick "$type"_popCount 0.5 > "$type"_ran.$version
$script/ranLoc "$type"_ran.$version ../../channels/"$type"_mag.dat.1 pop.in > "$type"_ranLoc.$version

echo "cat files"
cat "$young"/"$type"_ranLoc.$version "$old"/"$type"_ranLoc.$version > $merge/"$type"_ranLoc.$version

