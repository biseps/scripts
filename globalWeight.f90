PROGRAM globalWeight
	INTEGER :: i,j,lineCount,aerr,numFolders
	
	LOGICAL :: fileExists
      CHARACTER(len=1000) :: fin,fin2,in2,fileNum

      DOUBLE PRECISION,ALLOCATABLE,DIMENSION(:) :: res,tmp,num

	!WEB_weight
	call GETARG(1,fin)
	!Number of folders
	call GETARG(2,in2)
	READ(in2,'(I3)') numFolders

	
		fin2="1/"//fin(1:LEN_TRIM(fin))
		
		fileExists=.false.
		inquire(FILE=fin2,exist=fileExists)
		if(fileExists .eqv. .false.) then
			write(0,*) "No file 1 ", fin2
			stop
		end if
		
		OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
	
		lineCount=0
		DO WHILE (.true.)
			read(1, '(A)', end=99) BUFFER
			lineCount=lineCount+1
		ENDDO
	99    CONTINUE
		CLOSE(1)
		
	
	ALLOCATE(res(1:lineCount),tmp(1:lineCount),num(1:lineCount),STAT=aerr)
		IF (aerr > 0) THEN
			WRITE(0,*)
			WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
			WRITE(0,*)
			STOP 'Abnormal program termination!!!'
		END IF

	res=0.d0
	DO j=1,numFolders
	
		IF (j < 10) THEN
        		write(fileNum,'(I1)') j
        	ELSE IF (j < 100) THEN
         		write(fileNum,'(I2)') j
      	ELSE IF (j < 1000) THEN
        		write(fileNum,'(I3)') j
       	ELSE IF (j < 10000) THEN
       		write(fileNum,'(I4)') j
       	ELSE 
       		write(fileNum,'(I5)') j
      	END IF
		fin2=fileNum(1:LEN_TRIM(fileNum))//"/"//fin(1:LEN_TRIM(fin))
		
		fileExists=.false.
		inquire(FILE=fin2,exist=fileExists)
		if(fileExists .eqv. .false.) then
			write(0,*) "No file 2 ", fin2
			stop
		end if

		OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
		DO i=1,lineCount
			READ(1,*) tmp(i),num(i)
		END DO
		CLOSE(1)
		res=res+num
		write(0,*) j
	END DO
	
		DO i=1,lineCount
			write(*,*) tmp(i),res(i)
		END DO
	

END PROGRAM