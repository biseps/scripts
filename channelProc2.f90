PROGRAM channelProcess
!Take the WEB_pop_dat.1 file and matchs agaisnt the output of population so
!that we can see the numbers and types of systems
IMPLICIT NONE
	INTEGER :: i,j,k,lineCount,lineCount2,lineCount3,aerr

	INTEGER,ALLOCATABLE,DIMENSION(:) :: chan,typeNum
	DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:) :: num,typeSum,newSum
	CHARACTER(len=100), ALLOCATABLE,DIMENSION(:) :: typeChar,finalState,newState
	DOUBLE PRECISION :: tmp

	LOGICAL :: fileExists
      CHARACTER(len=100) :: fin,fin2,fin3
      CHARACTER(len=1000) :: BUFFER

	!{bin,eb}_type
	call GETARG(1,fin)

	fileExists=.false.
 	inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
      DO WHILE (.true.)
		read(1, '(A)', end=99) BUFFER
		lineCount=lineCount+1
  	ENDDO
99  	CONTINUE
    	CLOSE(1)

    	
    	aerr=0

	ALLOCATE(typeNum(1:lineCount),&
	typeChar(1:lineCount),typeSum(1:lineCount),newSum(1:lineCount),&
	finalState(1:lineCount),newState(1:lineCount),STAT=aerr)
	 IF (aerr > 0) THEN
            WRITE(0,*)
            WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(0,*)
            STOP 'Abnormal program termination!!!'
         END IF


    	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
    	READ(1,*) (typeNum(i),typeSum(i),typeChar(i), i=1,lineCount)
	CLOSE(1)


	DO i=1,lineCount
		finalState(i)=typeChar(i)(len_trim(typeChar(i))-3:len_trim(typeChar(i)))
	END DO

	newSum=0.d0
	do i=1,lineCount
	finalState(i)=trim(finalState(i))
	end do

	k=1
	newState(1)=finalState(1)
	newSum(1)=typeSum(1)
	outer: DO i=2,lineCount
 			Do j=1,k
        			if (newState(j) == finalState(i)) then
         		      	! Found a match so start looking again
         		  	 	newSum(j)=newSum(j)+typeSum(i)
          			 	cycle outer
        			end if
     			end do
     		! No match found so add it to the output
     		k = k + 1
     		newState(k)=finalState(i)
     		newSum(k)=typeSum(i)
  	end do outer

	do i=1,k
  	write(*,*) trim(newState(i)),newSum(i)
  	end do
	

END PROGRAM channelProcess