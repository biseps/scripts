PROGRAM mergeYoungOld
	IMPLICIT NONE
	INTEGER :: i,lineCount,aerr
	
	DOUBLE PRECISION, ALLOCATABLE, DIMENSION(:) :: Num_y,Num_o,Pop_y,Pop_o
	LOGICAL :: fileExists
    CHARACTER(len=100) :: fin,fin2
    CHARACTER(len=1000) :: BUFFER
	
	
	call GETARG(1,fin)
	call GETARG(2,fin2)

	fin=fin(1:LEN_TRIM(fin))
	fin2=fin2(1:LEN_TRIM(fin2))
	
	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
 	
 	fileExists=.false.
 	inquire(FILE=fin2,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if
 	
 	
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
    DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  			lineCount=lineCount+1
    ENDDO
99  CONTINUE
    CLOSE(1)
    
    ALLOCATE(Num_y(1:lineCount),Num_o(1:lineCount),Pop_y(1:lineCount),Pop_o(1:lineCount),STAT=aerr)
         IF (aerr > 0) THEN
            WRITE(*,*)
            WRITE(*,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(*,*)
            STOP 'Abnormal program termination!!!'
         END IF
         
         
    OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
    	READ(1,*) (Num_y(i),Pop_y(i), i=1,lineCount)
	CLOSE(1)
	
	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
    	READ(1,*) (Num_o(i),Pop_o(i), i=1,lineCount)
	CLOSE(1)
	
	DO i=1,lineCount
		write(*,*) Num_y(i),Pop_y(i)+Pop_o(i)
	END DO
	
END PROGRAM mergeYoungOld
