PROGRAM ranPick
	
	 USE nr, ONLY : ran1
	 USE ran_state, ONLY : ran_seed
	IMPLICIT NONE

	INTEGER :: i,j,k,lineCount,aerr,intTotalProb,numTries=1000
	INTEGER :: minID,midID,maxID,clock
	INTEGER,ALLOCATABLE,DIMENSION(:) :: result
	
	DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:) :: num,prob,culSum
	DOUBLE PRECISION :: totalProb,ran

	LOGICAL :: fileExists
      CHARACTER(len=100) :: fin,fin2,fileOut,fileOut2,fileNum
      CHARACTER(len=1000) :: BUFFER
    
      CHARACTER(len=1000),ALLOCATABLE,DIMENSION(:) :: evolData

	call GETARG(1,fin)

	fileExists=.false.
 	inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
      DO WHILE (.true.)
		read(1, '(A)', end=99) BUFFER
		lineCount=lineCount+1
  	ENDDO
99  	CONTINUE
    	CLOSE(1)
    	aerr=0

    	ALLOCATE(num(1:lineCount),prob(1:lineCount),culSum(0:lineCount))
         IF (aerr > 0) THEN
            WRITE(0,*)
            WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(0,*)
            STOP 'Abnormal program termination!!!'
         END IF
      
         
    	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
    	READ(1,*) (num(i),prob(i), i=1,lineCount)
	CLOSE(1)
	
	!Make running total of probailities
	culSum(0)=0
	DO i=1,lineCount
		culSum(i)=culSum(i-1)+prob(i)
	END DO
	
	!Extract the total
	totalProb=culSum(lineCount)
	!Round to nearest int
	intTotalProb=nint(totalProb)

!  	write(0,*) totalProb,intTotalProb
	
	!Scale everything to be between 0 and 1
	culSum=culSum/totalProb

! 	write(0,*) culSum(lineCount)
          
    	! Initialize random number generator
    	CALL SYSTEM_CLOCK(COUNT=clock)
    	CALL ran_seed(sequence=clock)

	i=intTotalProb

    	ALLOCATE(result(1:numTries*intTotalProb))
         IF (aerr > 0) THEN
            WRITE(0,*)
            WRITE(0,*) '1, ERROR ALLOCATING MEMORY2!'
            WRITE(0,*)
            STOP 'Abnormal program termination!!!'
         END IF


!       DO j=1,numTries
      	result=0
		!Loop until we have generted all stars needed
		DO i=1,intTotalProb*numTries
		
			!Generate random number in range 0 to 1
			CALL ran1(ran)
			!Search culSum for where ran is within element i and i-1
		
			minID=1
			maxID=lineCount
			binarySearch : DO
				midID=floor(dble(maxID+minID)/2)
				IF(ran .ge. culSum(midID-1) .and. ran .lt. culSum(midID))THEN
					result(i)=midID
					EXIT binarySearch
				ELSE IF(ran .lt. culSum(midID-1)) THEN
					maxID=midID
				ELSE
					minID=midID
				END IF
		
			END DO binarySearch
		
		END DO

! 		IF (j < 10) THEN
!         		write(fileNum,'(I1)') j
!         	ELSE IF (j < 100) THEN
!          		write(fileNum,'(I2)') j
!       	ELSE IF (j < 1000) THEN
!         		write(fileNum,'(I3)') j
!        	ELSE IF (j < 10000) THEN
!        		write(fileNum,'(I4)') j
!        	ELSE 
!        		write(fileNum,'(I5)') j
!       	END IF
! 
! 		OPEN(1,FILE=fileNum,STATUS='new',ACTION='write')
    	
! 		CLOSE(1)
! 		write(*,*) "Done",j
! 	end DO

	! 		CALL heapsort(intTotalProb,result)
		DO i=1,intTotalProb*numTries
			write(*,*) result(i)
		END DO


END PROGRAM ranPick

SUBROUTINE heapsort(n,ra)
	IMPLICIT NONE
	INTEGER,INTENT(IN) :: n
	INTEGER,INTENT(INOUT),DIMENSION(1:n) :: ra
	INTEGER :: i,ir,j,l
	INTEGER :: rra

	if(n.lt.2) return
	l=n/2+1
	ir=n
10	continue
		if(l.gt.1)then
			l=l-1
			rra=ra(l)
		else
			rra=ra(ir)
			ra(ir)=ra(1)
			ir=ir-1
			if(ir.eq.1)then
				ra(1)=rra
				return
			endif
		endif
		i=l
		j=l+1
		DO WHILE (j.le.ir)
			if(j.lt.ir)then
				if(ra(j).lt.ra(j+1))j=j+1
			endif
			if(rra.lt.ra(j))then
				ra(i)=ra(j)
				i=j
				j=l+j
			else
				j=ir+1
			endif
		END DO
		ra(i)=rra
	goto 10
END SUBROUTINE heapsort
	


