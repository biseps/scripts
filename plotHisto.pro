.reset
file='/padata/beta/users/rfarmer/data/kepler30/channels/WEB_kepler.dat.2'
openr,lun,file,/GET_LUN
nrowsMain=file_lines(file)
dataStruct={num:0.0,m1:0.0,r1:0.0,t1:0.0,l1:0.0,mag1:0.0,m2:0.0,r2:0.0,t2:0.0,l2:0.0,mag2:0.0,porb:0.0,birth:0.0,death:0.0,massT:0}
dataMain=replicate(dataStruct,nrowsMain)
readf,lun,dataMain

file='WEB_ran_eb'
openr,lun,file,/GET_LUN
nrowsPop=file_lines(file)
dataPop=fltarr(nrowsPop)
readf,lun,dataPop

dataSub=replicate(dataStruct,nrowsPop)

dataSub=dataMain[dataPop]


numBins=100.0
minBin=MIN(dataMain2) < min(dataSub2)
maxBin=MAX(dataMain2) > max(dataSub2)
binsize=(maxBin-minBin)/(numbins-1.0)
;maxBin=4.5

histoMain=histogram(dataMain2,BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64,REVERSE_INDICES=histoMainInd,LOCATIONS=mainHistoLoc)

histoMainTot=fltarr(numBins)
newLoc=fltarr(numBins)

histoMainIndTot=0

FOR i=0,numBins-2 DO BEGIN &$
IF histoMainInd[i] NE histoMainInd[i+1] THEN histoMainTot[i]=TOTAL(dataPop[histoMainInd(histoMainInd[i] : histoMainInd[i+1]-1)].weight) &$
ENDFOR

FOR i=0,numBins-2 DO BEGIN &$
newloc[i]=(mainHistoLoc[i]+mainHistoLoc[i+1])/2.0 &$
ENDFOR
newloc[numBins-1]=maxBin

newloc[numBins-1]=maxBin
dataSubHist=histogram(dataSub2,BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64)

PS_OPEN,'mass'+'.ps'
cghistoplot,dataSub2,BINSIZE=binsize,MININPUT=minBin,MAXINPUT=maxBin,/L64,/FREQUENCY,/WINDOW,xtitle='Log10(Mass)'

cgplot,newloc,histoMainTot/(total(histoMainTot)*1.0),/OVERPLOT,/WINDOW
PS_CLOSE
