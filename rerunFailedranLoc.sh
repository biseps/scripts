#!/bin/bash

script="/padata/beta/users/rfarmer/code/scripts"
source="/padata/beta/users/rfarmer/code/source"
for i in $(seq 1 84);do
	line1=$(wc -l $i/WEBwide_ranLoc.1 2> /dev/null | awk '{print $1}')
	for j in $(seq 2 12)
	do
		line=$(wc -l $i/WEBwide_ranLoc.$j 2> /dev/null | awk '{print $1}')

		if [[ $line1 != $line ]]; then
			echo $i $j "start"
			$script/ranPick $i/WEBwide_popCount > $i/WEBwide_ran.$j
			$source/ranLoc $i/WEBwide_ran.$j ../channels/WEBwide_mag.dat.1 $i/pop.in > $i/WEBwide_ranLoc.$j
			echo $i $j "done"
		fi
	done
done
