PROGRAM weightTrans
	
	IMPLICIT NONE

	INTEGER :: i,j,k,lineCount,aerr,intTotalProb
	INTEGER,ALLOCATABLE,DIMENSION(:) :: result
	
	DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:) :: num,prob,trans
	DOUBLE PRECISION :: tmp,x,y

	LOGICAL :: fileExists
	CHARACTER(len=100) :: fin,fin2
	CHARACTER(len=1000) :: BUFFER

	!WEB_merge
	call GETARG(1,fin)
	!EB/transits/{EB,Cat}.txt
	call GETARG(2,fin2)

	fileExists=.false.
	inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
	if(fileExists .eqv. .false.) then
		write(0,*) "No file ", fin
		stop
	end if

	fileExists=.false.
	inquire(FILE=fin2(1:LEN_TRIM(fin2)),exist=fileExists)
	if(fileExists .eqv. .false.) then
		write(0,*) "No file ", fin2
		stop
	end if
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
	DO WHILE (.true.)
		read(1, '(A)', end=99) BUFFER
		lineCount=lineCount+1
	ENDDO
99  	CONTINUE
	CLOSE(1)
	aerr=0


	ALLOCATE(num(1:lineCount),prob(1:lineCount),trans(1:lineCount))
	IF (aerr > 0) THEN
		WRITE(0,*)
		WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
		WRITE(0,*)
		STOP 'Abnormal program termination!!!'
	END IF
	
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
	READ(1,*) (num(i),prob(i), i=1,lineCount)
	CLOSE(1)

	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount
		READ(1,*) tmp,x,y
		!Min prob => max inclination
		trans(i)=MIN(x,y)
	END DO
	CLOSE(1)

	DO i=1,lineCount
		WRITE(*,*) num(i),prob(i)*trans(i)
	END DO
	
END PROGRAM weightTrans