PROGRAM extractLines
	IMPLICIT NONE
	INTEGER :: i,j,lineCount1,lineCount2,aerr,lineCount3
	
	LOGICAL :: fileExists
      CHARACTER(len=100) :: fin,fin2,fin3
      CHARACTER(len=1000) :: BUFFER

      INTEGER,ALLOCATABLE,DIMENSION(:) :: inputLines

      TYPE :: star
		DOUBLE PRECISION :: num,m1,m2,r1,r2,t1,t2,l1,l2,mag1,mag2
		DOUBLE PRECISION :: birth,death,porb
		CHARACTER(len=12) :: spec1,spec2,chn
		INTEGER :: mass
      END TYPE star

      TYPE(star),ALLOCATABLE, DIMENSION(:) :: s
      DOUBLE PRECISION,ALLOCATABLE,DIMENSION(:) :: trans
      DOUBLE PRECISION :: tmp,x,y
	
	call GETARG(1,fin)
	call GETARG(2,fin2)
	call GETARG(3,fin3)

	!WEB_merge
	fin=fin(1:LEN_TRIM(fin))
	!channels/WEB_kepler
	fin2=fin2(1:LEN_TRIM(fin2))
	!EB/tranists/{EB,Cat}.txt
	fin3=fin3(1:LEN_TRIM(fin3))
	
	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
 	
 	fileExists=.false.
 	inquire(FILE=fin2,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if
 	
 	fileExists=.false.
 	inquire(FILE=fin3,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin3
 		stop
 	end if
 	
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount1=0
	DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  		lineCount1=lineCount1+1
      ENDDO
99    CONTINUE
      CLOSE(1)

      OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')

	lineCount2=0
	DO WHILE (.true.)
  		read(1, '(A)', end=98) BUFFER
  		lineCount2=lineCount2+1
      ENDDO
98    CONTINUE
      CLOSE(1)

      OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')

	lineCount3=0
	DO WHILE (.true.)
  		read(1, '(A)', end=97) BUFFER
  		lineCount3=lineCount3+1
      ENDDO
97    CONTINUE
      CLOSE(1)

      IF(lineCount1 .ne. lineCount2 .or. lineCount1 .ne. lineCount3)THEN
      write(*,*) "Inputs not aligned", lineCount1,lineCount2,lineCount3
      END IF
    
      ALLOCATE(inputLines(1:lineCount1),s(1:lineCount2),trans(1:lineCount3),STAT=aerr)
         IF (aerr > 0) THEN
            WRITE(*,*)
            WRITE(*,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(*,*)
            STOP 'Abnormal program termination!!!'
         END IF

	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount1
		READ(1,*) inputLines(i)
	END DO
	CLOSE(1)

	OPEN(1,FILE=fin3,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount1
		READ(1,*) tmp,x,y
		!Pick smallest prob which = biggest incliantion
		trans(i)=MIN(x,y)
	END DO
	CLOSE(1)

	inputLines=inputLines*trans

	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount2
		READ(1,*) s(i)%num,s(i)%m1,s(i)%r1,s(i)%t1,s(i)%l1,s(i)%mag1,&
		s(i)%m2,s(i)%r2,s(i)%t2,s(i)%l2,s(i)%mag2,s(i)%porb,s(i)%birth,s(i)%death,s(i)%mass,s(i)%spec1,&
		s(i)%spec2,s(i)%chn
	END DO
	CLOSE(1)

	DO i=1,lineCount1
		j=inputLines(i)
		WRITE(*,'(F12.4,1X,13(E18.12,1X),I3,1X,A,1X,A,1X,A,1X)') s(j)%num,s(j)%m1,s(j)%r1,s(j)%t1,s(j)%l1,s(j)%mag1,&
		s(j)%m2,s(j)%r2,s(j)%t2,s(j)%l2,s(j)%mag2,s(j)%porb,s(j)%birth,s(j)%death,s(j)%mass,s(j)%spec1,&
		s(j)%spec2,s(j)%chn
	END DO


END PROGRAM extractLines
