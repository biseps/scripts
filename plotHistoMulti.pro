;.reset

input = COMMAND_LINE_ARGS()
;fileNum='21'
fileNum=input[0]

file='WEB_extract.2'
openr,lun,file,/GET_LUN
dataStruct={num:0.0,m1:0.0,r1:0.0,t1:0.0,l1:0.0,mag1:0.0,m2:0.0,r2:0.0,t2:0.0,l2:0.0,mag2:0.0,porb:0.0,birth:0.0,death:0.0,massT:0}
nrowsExt=file_lines(file)
dataSub=replicate(dataStruct,nrowsExt)
readf,lun,dataSub
close,lun

file='/padata/beta/users/rfarmer/data/kepler35/channels/WEB_kepler.dat.2'
openr,lun,file,/GET_LUN
nrowsMain=file_lines(file)
dataStruct={num:0.0,m1:0.0,r1:0.0,t1:0.0,l1:0.0,mag1:0.0,m2:0.0,r2:0.0,t2:0.0,l2:0.0,mag2:0.0,porb:0.0,birth:0.0,death:0.0,massT:0}
dataMain=replicate(dataStruct,nrowsMain)
readf,lun,dataMain
FREE_LUN,lun

file='WEB_merge2'
openr,lun,file,/GET_LUN
nrows=file_lines(file)
dataStruct2={num:0.0,weight:0.0}
dataPop=replicate(dataStruct2,nrows)
readf,lun,dataPop
FREE_LUN,lun


file="ran/1"
numFiles=1000
openr,lun,file,/GET_LUN
nrows=file_lines(file)
tmpArr=fltArr(nrows)
massRan=fltarr(numFiles,nrows)
readf,lun,tmpArr
massRan[0,*]=dataMain[tmpArr].m1
FREE_LUN,lun

FOR i=1,numFiles-1 DO BEGIN &$
	tmpArr=fltArr(nrows) &$
	file=strtrim('ran/' + strtrim(STRING(i+1),2),2) &$
	openr,lun,file,/GET_LUN &$
	readf,lun,tmpArr &$

	massRan[i,*]=dataMain[tmpArr].m1 &$
	FREE_LUN,lun &$
ENDFOR

massRan=alog10(massRan)

;dataSub2=((dataSub.r1+dataSub.r2)/aSub)
dataSub2=alog10(dataSub.m1)
;dataSub2=alog10(dataSub.t1)

;dataMain2=((dataMain.r1 + dataMain.r2)/(aMain))
dataMain2=alog10(dataMain.m1)
;dataMain2=alog10(dataMain.t1)

numBins=100.0
minBin=MIN(dataMain2) < min(dataSub2) < min(massRan)
maxBin=Max(dataMain2) > max(dataSub2) > max(massRan)
;maxBin=7.0
binsize=(maxBin-minBin)/(numbins-1.0)

histoMain=histogram(dataMain2,BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64,REVERSE_INDICES=histoMainInd,LOCATIONS=mainHistoLoc)

histoMainTot=fltarr(numBins)
histoMainErr=fltarr(numBins)
newLoc=fltarr(numBins)

FOR i=0,numBins-2 DO BEGIN &$
	IF histoMainInd[i] NE histoMainInd[i+1] THEN BEGIN &$
		histoMainTot[i]=TOTAL(dataPop[histoMainInd(histoMainInd[i] : histoMainInd[i+1]-1)].weight) &$
		histoMainErr[i]=N_ELEMENTS(histoMainInd(histoMainInd[i] : histoMainInd[i+1]-1)) &$
	ENDIF &$
ENDFOR

FOR i=0,numBins-2 DO BEGIN &$
	newloc[i]=(mainHistoLoc[i]+mainHistoLoc[i+1])/2.0 &$
ENDFOR
newloc[numBins-1]=maxBin

massInd=intarr(numFiles,numBins)
for i=0,numFiles-1 DO BEGIN &$
	tmp=fltarr(numBins) &$
	tmp=histogram(massRan[i,*],BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64,REVERSE_INDICES=R) &$
	FOR j=0,numBins-2 DO BEGIN &$
		IF R[j] NE R[j+1] THEN massInd[i,j]=N_ELEMENTS(massRan[R[R[j]:R[j+1]-1]])&$
	ENDFOR &$

ENDFOR

massInd=massInd/(nrows*1.0)

massAvg=fltarr(numBins)
massStdDev=fltarr(numBins)
for i=0,numBins-1 DO BEGIn &$
	massAvg[i]=MEAN(massInd[*,i]) &$
	massStdDev[i]=STDDEV(massInd[*,i]) &$
ENDFOR

dataSubHist=histogram(dataSub2,BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64)

maxYAxis=(MAX(massAvg+massstddev) > Max(dataSubHist)/(total(dataSubHist)*1.0)) > Max(histoMainTot/(total(histoMainTot)*1.0))
maxYaxis=maxYaxis*1.05

histoMainErr=sqrt(histoMainErr[WHERE(histoMainErr gt 0.0)])/(total(histoMainTot)*1.0)

PS_OPEN,'mass1'+'.ps'
cghistoplot,dataSub2,BINSIZE=binsize,MININPUT=minBin,MAXINPUT=maxBin,/L64,/FREQUENCY,/WINDOW,xtitle='Log10(Mass)',MAX_VALUE=maxYAxis,title=fileNum

histoMainTot=histoMainTot/(total(histoMainTot)*1.0)

cgplot,newloc,histoMainTot,/OVERPLOT,/WINDOW

; cgplot,newLoc,histoMainTot+histoMainErr,/OVERPLOT,/WINDOW,linestyle=3
; cgplot,newLoc,histoMainTot-histoMainErr,/OVERPLOT,/WINDOW,linestyle=3

cgplot,newLoc,massAvg,/OVERPLOT,/WINDOW,psym=4

cgplot,newLoc,massAvg+massstddev,/OVERPLOT,/WINDOW,linestyle=4
cgplot,newLoc,massAvg-massstddev,/OVERPLOT,/WINDOW,linestyle=4


al_Legend, ['Sub-Sample ='+strtrim(STRING(nrowsext),2), 'Intrinsic ='+strtrim(STRING(nrowsMain),2)], /right_legend,/top_legend, /Window

PS_CLOSE
END