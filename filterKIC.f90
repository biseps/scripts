module loggg
	contains
	ELEMENTAL DOUBLE PRECISION FUNCTION calcLogg(m,r)
	USE consts
	IMPLICIT NONE
	DOUBLE PRECISION,INTENT(IN) :: m,r

	calcLogg=log10(100.d0*(NewtGrav*m*Msun)/(r*r*Rsun*Rsun))

	END FUNCTION calcLogg


end module loggg

PROGRAM keplerFilter
	use loggg
!Will weigh each system based on whether it is inside the KIC at the various
!condtions
	IMPLICIT NONE
	INTEGER :: i,j,k,lineCount,lineCount2,lineCount3,aerr,tmpInt
	INTEGER :: flag13,flag14,flag15,flag16
	INTEGER,ALLOCATABLE,DIMENSION(:) :: id
	DOUBLE PRECISION ::tmp,tmp2
	DOUBLE PRECISION :: m,rad,t,l,ms
	DOUBLE PRECISION,DIMENSION(1:3) :: ttr,nt
	DOUBLE PRECISION,DIMENSION(1:3,1:11)  :: rMin
	DOUBLE PRECISION,DIMENSION(1:13) :: magWeight
	DOUBLE PRECISION,DIMENSION(1:15) :: cond
	DOUBLE PRECISION,DIMENSION(1:12) :: fluxNum,r,magD,numD,p,fkep,fbg,pStar,pBg,sigma,fkep2
	DOUBLE PRECISION,PARAMETER,DIMENSION(1:12) ::apSize=(/150.0,150.0,100.0,50.0,40.0,30.0,20.0,15.0,10.0,5.0,5.0,5.0/)
	CHARACTER(len=100) :: fin,fin2,fin3
	CHARACTER(len=1000) :: BUFFER,BUFFER2
	DOUBLE PRECISION :: r8,r13,r14,r15,r16
	LOGICAL :: fileExists

      DOUBLE PRECISION,PARAMETER :: pi = 3.141592654d0,TWOPI=2.d0*pi
	DOUBLE PRECISION,PARAMETER :: pc=1.2,tint=6.0,na=1.0,nr=120.0,nfr=270.0,f12=1.74d5
	DOUBLE PRECISION,PARAMETER :: pixCnt=1126400.0,rEarth=6.378136d6
	DOUBLE PRECISION,PARAMETER :: au=1.4986d11,Msun=1.98892d30,Rsun=6.955d8,g=6.673d-11,lsun=3.862d26

	TYPE :: system
		DOUBLE PRECISION ::num
		DOUBLE PRECISION,DIMENSION(1:2) :: mt,reff,teff,lum
		DOUBLE PRECISION :: porb,mag,weight,logg
	END TYPE system

	TYPE(system),ALLOCATABLE,DIMENSION(:) :: s,sRaw

	!WEb_kepler.dat.1
	CALL GETARG(1,fin)
	!File with numbers per mag
	CALL GETARG(2,fin2)
	!WEB_merge
	CALL GETARG(3,fin3)

	fin=fin(1:len_trim(fin))
	fin2=fin2(1:len_trim(fin2))
	fin3=fin3(1:len_trim(fin3))
	
	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if

	fileExists=.false.
 	inquire(FILE=fin2,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if
 		
 	fileExists=.false.
 	inquire(FILE=fin3,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin3
 		stop
 	end if

 	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
	DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  		lineCount=lineCount+1
      ENDDO
99    CONTINUE
      CLOSE(1)
	
	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
	lineCount2=0
	DO WHILE (.true.)
  		read(1, '(A)', end=98) BUFFER
  		lineCount2=lineCount2+1
      ENDDO
98    CONTINUE
      CLOSE(1)

      OPEN(1,FILE=fin3,STATUS='OLD',ACTION='READ')
	lineCount3=0
	DO WHILE (.true.)
  		read(1, '(A)', end=97) BUFFER
  		lineCount3=lineCount3+1
      ENDDO
97    CONTINUE
      CLOSE(1)

      if(lineCount.ne.lineCount3) then
            WRITE(*,*)'1, Lines dont match!'
            WRITE(*,*)lineCount,lineCount3
            WRITE(*,*)fin,fin3
            STOP 'Abnormal program termination!!!'
         END IF      


	!Read files
	aerr=0
	ALLOCATE(s(1:lineCount),STAT=aerr)
         IF (aerr > 0) THEN
            WRITE(*,*)
            WRITE(*,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(*,*)
            STOP 'Abnormal program termination!!!'
         END IF
	
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount
      READ(1,'(F12.4,1X,11(E18.12,1X),I3,A)') s(i)%num,s(i)%mt(1),s(i)%reff(1),s(i)%teff(1),s(i)%lum(1),&
      							s(i)%mt(2),s(i)%reff(2),s(i)%teff(2),s(i)%lum(2),&
      							s(i)%porb,tmp,tmp,tmpInt,BUFFER
     	END DO						
	CLOSE(1)

	s%logg=calcLogg(s%mt(1),s%reff(1))

	OPEN(1,FILE=fin3,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount
      	READ(1,*) tmp,s(i)%weight
     	END DO						
	CLOSE(1)

	!Read in mag distribution file
	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
		READ(1,*) (magD(i),numD(i),i=1,12)
	CLOSE(1)

	!Generate CDF of magD
	magWeight(1)=0.0
	DO i=2,8
		magWeight(i)=numD(i)/SUM(numD(1:8))
	END DO

! 	write(*,*) magWeight(1:8)
! 	stop

	
	
! 	!p is the noise from http://keplergo.arc.nasa.gov/CalibrationSN.shtml
! 	!1 is 8 mag; 2 is 13; 3 14;4 15; 5 16
	fkep=10**(-0.4*((magD-0.5)-12.0))*f12
	
 	!Calculate r as fstar/(fstar+fbg)
 	!this is the flux of star at mag multipled by number of objects at that mag
	fluxNum=(10**(-magD/2.5))*numD

! Doesnt work as background flux is uniform but for the optiaml aperature is a func of magnitude and sky positon
!and relies on the discreate noise present in the background
! !Calculate the optimal aperature for each magnitude
! ! http://adsabs.harvard.edu/abs/2010ApJ...713L..92C
! ! http://adsabs.harvard.edu/abs/2010ApJ...713L..97B
! 
! !Calculate the background flux per pixel from all stars fainter than target
! 	DO  i=1,11
! 		DO j=1,100
! 			fbg(i)=SUM(fluxNum(i+1:))*f12*(j/pixCnt)
! 
! 			fkep2(i)=10**(-0.4*((i+7.0)-12.0))*f12
! 		!Calc p as a per pixel number for the star
! 			pStar(i)=pc*sqrt((fkep2(i)*tint)+(j*(nr*nr)))/(sqrt(nfr)*fkep2(i)*tint)
! 		!For background
! 			pBg(i)=pc*sqrt((fbg(i)*tint)+(j*(nr*nr)))/(sqrt(nfr)*fbg(i)*tint)
! 		!Add everything up
! 			sigma(i)=sqrt(pStar(i)+pBg(i)+nr**2+(pStar(i)*sqrt(12.0)/4.0)**2)
! 
! 		write(*,*) pStar(i)/sigma(i)
! 		END DO
! ! 		stop
! 	END DO
! 	stop
! 
! 	write(*,*) fkep
! 	stop
 	
	p=pc*sqrt((fkep*tint)+(apSize*(nr*nr)))/(sqrt(nfr)*fkep*tint)

! 	write(*,*) p*10**(6)
! 	stop
! 
! 	!Go from 8 to 19 (only actualy need up to 16)
! 	! We define aperature to be 20 pixels and the total size of a kepler ccd is
! 	!1044-20 for the masked array * 2200*0.5 as we run per channel
	r=0.0
	DO i=1,12
		ms=i+7.0
		tmp=0.d0
		DO j=i,12
			tmp=tmp+(10**((magD(j)-ms)/(-2.5)))*numD(j)
		END DO
		
		r(i)=1.0/(1.0+tmp*(apSize(i)/pixCnt))
! 		write(*,*) SUM(10**((magD(i:)-ms)/(-2.5))),SUM(numD(i:))
	END DO
!  	write(*,*) r
! 	stop
      DO i=1,lineCount
      	cond=0.0

		if(s(i)%lum(1).ge.s(i)%lum(2))then
			m=s(i)%mt(1)*msun
			rad=s(i)%reff(1)*rsun
			t=s(i)%teff(1)
			l=s(i)%lum(1)
		else
			m=s(i)%mt(2)*msun
			rad=s(i)%reff(2)*rsun
			t=s(i)%teff(2)
			l=s(i)%lum(2)
		end if

		if(s(i)%logg .lt. 0.0 .or. s(i)%logg .gt. 5.5 .or. t .lt. 3500.0 .or. t .gt. 55000.0)THEN
   			write(*,*) "0 ","0.0"
			cycle
		endif
		
		
		!Ignore systems where HZ is < 5Rstar
! 		IF(0.95*au*l.lt.5.0*rad)THEN
! !   			write(*,*) "0 ","0.0"
! 			cycle
! 		endif
 
		!L already is lstar/lsun
		!5R
		ttr(1)=2.0*rad*sqrt((5.0*rad)/(g*m))
		!0.5HZ
		ttr(2)=2.0*rad*sqrt((0.5*0.95*au*l)/(g*m))
		!hz
		ttr(3)=2.0*rad*sqrt((0.95*au*l)/(g*m))
 
 		!Now divide by 30 mins for number of transits in that time
		ttr=ttr/(30.0*60.0)
 
		nt(1)=sqrt(((5.0*rad)**3.0)/(g*m))*TWOPI
		nt(2)=sqrt(((0.5*0.95*au*l)**3.0)/(g*m))*TWOPI
		nt(3)=sqrt(((0.95*au*l)**3.0)/(g*m))*TWOPI
 
 		nt=nt/(60.0*60.0*24.0*365.0)
 		nt=3.5/nt

		!Now for the checking
		rMin=0.0
		FORALL(k=1:11)
			FORALL(j=1:3)
				rMin(j,k)=7.1*(p(k)/(sqrt(ttr(j))*sqrt(nt(j))))/r(k)
			END FORALL
		END FORALL

		rMin=(sqrt(rmin)*rad)/rEarth
		cond=0.0

! 		write(*,*) rMin
! 		stop
		
! 		write(*,*)
! 		write(*,*) p
! 		write(*,*)
! 		write(*,*) ttr
! 		write(*,*)
! 		write(*,*) r
! 		write(*,*)
! 		write(*,*) rad,rEarth
! 		stop
	
! 		if(nt(3) .lt. 3.0 ) cycle
! 
! 		write(*,*) 3.5/nt
! 		write(*,*) 5.0*rad/rsun,0.5*0.95*au*l/rsun,0.95*au*l/rsun
! 		write(*,*)

		flag13=0;flag14=0;flag15=0;flag16=0

! 		write(*,*) nt(3),rMin(3,1),rMin(3,6),"1"
! 		write(*,*) nt(3),rMin(3,1),rMin(3,6),"2"
! 		write(*,*) nt(3),rMin(3,6),rMin(3,7),"3"
! 		write(*,*) nt(3),rMin(3,6),rMin(3,7),"4"
! 		write(*,*) nt(3),rMin(3,7),rMin(3,8),"5"
! 		write(*,*) nt(3),rMin(3,8),rMin(3,9),"6"
! 		write(*,*) nt(2),rMin(3,1),rMin(3,7),"7"
! 		write(*,*) nt(2),rMin(3,1),rMin(3,7),"8"
! 		write(*,*) nt(1),rMin(3,1),rMin(3,7),"9"
! 		write(*,*) nt(3),rMin(3,7),rMin(3,8),"10"
! 		write(*,*) nt(3),rMin(3,8),rMin(3,9),"11"
! 		write(*,*) nt(1),rMin(3,7),rMin(3,8),"12"
! 		write(*,*) nt(1),rMin(3,8),rMin(3,9),"13"
! 		write(*,*) (1.0-MIN(rMin(3,1),1.0))/(rMin(3,6)-rMin(3,1)),(MAX(1.0,MIN(rMin(3,6),2.0))-1.0)/(rMin(3,6)-rMin(3,1))
! 		write(*,*) s(i)%weight

		IF(nt(3).ge.3.0.and.(rMin(3,1).le.2.0.or.rMin(3,6).le.2.0)) THEN
			tmp=0.d0
			DO j=1,5
				if(rMin(3,j+1).gt.1.0.and.rMin(3,j).gt.1.0) then
					tmp2=0.d0
				else if(rMin(3,j+1).gt.1.0) then
					tmp2=(1.0-rMin(2,j))/(Min(2.0,rMin(3,j+1))-rMin(3,j))
				else
					tmp2=1.d0
				end if
				tmp=tmp+(magWeight(j)*tmp2)
			END DO
			write(*,*) "1",tmp*s(i)%weight

			
			tmp=0.d0
			DO j=1,5
				if(rMin(3,j+1).lt.1.0.and.rMin(3,j).lt.1.0)then
					 tmp2=0.d0
				else if(rMin(3,j).lt.1.0) then
					tmp2=(rMin(3,j+1)-1.0)/(Min(2.0,rMin(3,j+1))-rMin(3,j))
				else
					tmp2=1.d0
				end if
				tmp=tmp+(magWeight(j)*tmp2)
			END DO
			write(*,*) "2",tmp*s(i)%weight
			flag13=1
		END IF


		IF(nt(3).ge.3.0.and.(rMin(3,6).le.2.0.or.rMin(3,7).le.2.0)) THEN
			tmp=0.d0
			j=6
				if(rMin(3,j+1).gt.1.0.and.rMin(3,j).gt.1.0) then
					tmp2=0.d0
				else if(rMin(3,j+1).gt.1.0) then
					tmp2=(1.0-rMin(3,j))/(Min(2.0,rMin(3,j+1))-rMin(3,j))
				else
					tmp2=1.d0
				end if
				
				tmp=tmp+(magWeight(j)*tmp2)
			write(*,*) "3",tmp*s(i)%weight
			
			tmp=0.d0
				if(rMin(3,j+1).lt.1.0.and.rMin(3,j).lt.1.0) then
					tmp2=0.d0
				else if(rMin(3,j).lt.1.0) then
					tmp2=(rMin(3,j+1)-1.0)/(Min(2.0,rMin(3,j+1))-rMin(3,j))
				else
					tmp2=1.d0
				end if
				tmp=tmp+(magWeight(j)*tmp2)
			write(*,*) "4",tmp*s(i)%weight
			flag14=1
		END IF


		IF(nt(3).ge.3.0.and.(rMin(3,7).lt.1.0.or.rMin(3,8).lt.1.0))THEN
			j=7
			if(rMin(2,j+1).gt.1.0.and.rMin(2,j).gt.1.0) then
				tmp2=0.d0
			else if(rMin(2,j+1).gt.1.0) then
				 tmp2=(1.0-rMin(2,j))/(Min(2.0,rMin(2,j+1))-rMin(2,j))
			else
				tmp2=1.d0
			end if
 			write(*,*) "5 ",magWeight(7)*s(i)%weight*tmp2
 			flag15=1
! 			cond(5)=magWeight(7)*s(i)%weight
		END IF	
		IF(nt(3).ge.3.0.and.(rMin(3,8).lt.1.0.or.rMin(3,9).lt.1.0))THEN
			j=8
			if(rMin(2,j+1).gt.1.0.and.rMin(2,j).gt.1.0) then
				tmp2=0.d0
			else if(rMin(2,j+1).gt.1.0) then
				tmp2=(1.0-rMin(2,j))/(Min(2.0,rMin(2,j+1))-rMin(2,j))
			else
				tmp2=1.d0
			end if
 			write(*,*) "6 ",magWeight(8)*s(i)%weight*tmp2
 			flag16=1
! 			cond(6)=magWeight(8)*s(i)%weight
		END IF

		IF(flag14.eq.0.and.nt(2).ge.3.0.and.(rMin(2,1).le.2.0.or.rMin(2,7).le.2.0)) THEN
			tmp=0.d0
			DO j=1,6
				if(rMin(2,j+1).gt.1.0.and.rMin(2,j).gt.1.0) then
					tmp2=0.d0
				else if(rMin(2,j+1).gt.1.0) then
					tmp2=(1.0-rMin(2,j))/(Min(2.0,rMin(2,j+1))-rMin(2,j))
				else
					tmp2=1.d0
				end if
			
				tmp=tmp+magWeight(j)*tmp2
			END DO
			write(*,*) "7",tmp*s(i)%weight
			
			tmp=0.d0
			DO j=1,6
				if(rMin(2,j+1).lt.1.0.and.rMin(2,j).lt.1.0) then
					tmp2=0.d0
				else if(rMin(2,j).lt.1.0) then
					tmp2=(rMin(2,j+1)-1.0)/(Min(2.0,rMin(2,j+1))-rMin(2,j))
				else
					tmp2=1.d0
				end if
			
				tmp=tmp+magWeight(j)*tmp2
			END DO
			write(*,*) "8",tmp*s(i)%weight
			flag14=1
		END IF
			
		IF(flag14.eq.0.and.nt(1).ge.3.0 .and.&
					   ((rMin(1,1).le.2.0).or.&
					   (rMin(1,7).le.2.0)))THEN
					   
 			write(*,*) "9 ",s(i)%weight*SUM(magWeight(1:6))
			flag14=1
! 			cond(9)=SUM(magWeight(1:6))*s(i)%weight
		END IF
		
		IF(nt(3).ge.3.0 .and.&
					   ((rMin(3,7).ge.1.0.and.rMin(3,7).le.2.0).or.&
				         (rMin(3,8).ge.1.0.and.rMin(3,8).le.2.0)))THEN
			j=7   
			if(rMin(2,j+1).gt.1.0.and.rMin(2,j).gt.1.0) then
				tmp2=0.d0
			else if(rMin(2,j+1).gt.1.0) then
				tmp2=(1.0-rMin(2,j))/(Min(2.0,rMin(2,j+1))-rMin(2,j))
			else
				tmp2=1.d0
			end if
 			write(*,*) "10 ",magWeight(7)*s(i)%weight*tmp2
 			flag15=1
! 			cond(10)=magWeight(7)*s(i)%weight
		END IF
		IF(nt(3).ge.3.0 .and.&
					   ((rMin(3,8).ge.1.0.and.rMin(3,8).le.2.0).or.&
					   (rMin(3,9).ge.1.0.and.rMin(3,9).le.2.0)))THEN

			j=8
			if(rMin(2,j+1).gt.1.0.and.rMin(2,j).gt.1.0) then
				tmp2=0.d0
			else if(rMin(2,j+1).gt.1.0) then
				tmp2=(1.0-rMin(2,j))/(Min(2.0,rMin(2,j+1))-rMin(2,j))
			else
				tmp2=1.d0
			end if
 			write(*,*) "11 ",magWeight(8)*s(i)%weight*tmp2
 			flag16=1
! 			cond(11)=magWeight(8)*s(i)%weight
		END IF		

		IF(flag15.eq.0.and.nt(1).ge.3.0 .and.&
					   ((rMin(1,7).le.2.0).or.&
					   (rMin(1,8).le.2.0)))THEN
 			write(*,*) "12 ",magWeight(7)*s(i)%weight
			flag15=1
!			cond(12)=magWeight(7)*s(i)%weight
		END IF
		IF(flag16.eq.0.and.nt(1).ge.3.0 .and.&
					   ((rMin(1,8).le.2.0).or.&
					   (rMin(1,9).le.2.0)))THEN
 			write(*,*) "13 ",magWeight(8)*s(i)%weight
			flag16=1
! 			cond(13)=magWeight(8)*s(i)%weight
		END IF

		if(flag13.eq.0.and.flag14.eq.0.and.flag15.eq.0.and.flag16.eq.0)THEN
			write(*,*) "0 0.0"
		end if

!    		stop
 	END DO 

END PROGRAM keplerFilter
