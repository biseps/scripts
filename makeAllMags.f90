PROGRAM makeAllMags
	USE dtypes
! 	USE magnitude
	USE params
	USE spectral
	USE BC
!Translates the abs mags calcauted in makeMag and the distance found in ranLoc
!to create app mag in filter bands
IMPLICIT NONE
	INTEGER(8) :: i,j,lineCount1,lineCount2,aerr,tmpInt,id
	
	LOGICAL :: fileExists
      CHARACTER(len=1000) :: fin,fin2
      CHARACTER(len=1000) :: BUFFER
      CHARACTER(len=1) :: inSing
      CHARACTER(len=10) :: inMet
      DOUBLE PRECISION :: tmp,num

      TYPE ranLoc
		DOUBLE PRECISION :: id,absKp,lran,bran,dran,ext,appKp
      END TYPE ranLoc
      TYPE(ranLoc), ALLOCATABLE,DIMENSION(:) :: loc
	TYPE(stq),DIMENSION(1:2):: s
	TYPE(bnq) :: b

	DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: magSing
	DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: magBin
	TYPE(spectralType),DIMENSION(1:2) :: spec
	DOUBLE PRECISION :: kp

!   	metallicity=0.02
!   	metallicity=0.0033
	filterBand='Kp'
! 	isSingle=0

	CALL preIntBC((/'310','311','312','313','314','505','355','356','357'/))

	!ranLoc
	call GETARG(1,fin)
	!._kepler from extract
	call GETARG(2,fin2)
	!Both must of been made without altering the ordering of the lines
	! as ranLoc id is the line number in kepler file while mags id is
	!the id generted by BiSEPS
	!this works:
! 	for i in $(seq 1 84);do cat ../../../kepler30/pop_s_y/$i/WEBwide_ranLoc.0.5a ../../../kepler30/pop_s_o/$i/WEBwide_ranLoc.0.5a >> ranLoc.sing.0.5a;echo $i;done

	call GETARG(3,inSing)
	read(inSing,'(i1)') isSingle

	call GETARG(4,inMet)
	read(inMet,'(F12.5)') metallicity

	fin=fin(1:LEN_TRIM(fin))
	fin2=fin2(1:LEN_TRIM(fin2))
	
	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
 	
 	fileExists=.false.
 	inquire(FILE=fin2,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if
 	
 	
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount1=0
	DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  		lineCount1=lineCount1+1
      ENDDO
99    CONTINUE
      CLOSE(1)

      OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')

	lineCount2=0
	DO WHILE (.true.)
  		read(1, '(A)', end=98) BUFFER
  		lineCount2=lineCount2+1
      ENDDO
 98    CONTINUE
      CLOSE(1)

      if(lineCount1 .ne. lineCount2) THEN
            WRITE(0,*)
            WRITE(0,*) 'Linecount not the same'
            WRITE(0,*) lineCount1,lineCount2
            STOP 
         END IF      

    aerr=0
      ALLOCATE(loc(1:lineCount1),STAT=aerr)
         IF (aerr > 0) THEN
            WRITE(0,*)
            WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(0,*)
            STOP 'Abnormal program termination!!!'
         END IF


	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount1
		READ(1,*) loc(i)%id,loc(i)%absKp,loc(i)%lran,&
		loc(i)%bran,loc(i)%dran,loc(i)%ext,loc(i)%appKp
	END DO
	CLOSE(1)


	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount1
		READ(1,*)	id,num,s(1)%mt,s(1)%reff,s(1)%teff,s(1)%lum,&
			s(2)%mt,s(2)%reff,s(2)%teff,s(2)%lum,&
			b%porb,tmp,tmp,tmpInt,BUFFER


		IF(int(id) .ne. int(loc(i)%id)) then
	            WRITE(0,*)
            	WRITE(0,*) 'Line ids dont match'
      	      WRITE(0,*) i,loc(i)%id,id
            	STOP
         	END IF
         	
		CALL getMagBC(s%mt,s%reff,s%teff,s%lum,s%kw,metallicity,isSingle,magSing,magBin,spec)


		loc(i)%ext=(loc(i)%ext/0.88)
	
		magBin(1)=magBin(1)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*1.565)
		magBin(2)=magBin(2)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*1.193)
		magBin(3)=magBin(3)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.868)
		magBin(4)=magBin(4)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.681)
		magBin(5)=magBin(5)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.490)
		magBin(6)=magBin(6)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.999)
		magBin(7)=magBin(7)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.290)
		magBin(8)=magBin(8)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.183)
		magBin(9)=magBin(9)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.115)

! 		magBin(1)=magBin(1)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*1.594)
! 		magBin(2)=magBin(2)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*1.181)
! 		magBin(3)=magBin(3)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.88)
! 		magBin(4)=magBin(4)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.666)
! 		magBin(5)=magBin(5)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.507)
! 		magBin(6)=magBin(6)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*1.269)
! 		magBin(7)=magBin(7)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.304)
! 		magBin(8)=magBin(8)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.209)
! 		magBin(9)=magBin(9)+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.136)


		!calc kp mag based on KIC code
		if(magBin(2)-magBin(3).gt.0.3) then
			kp=0.7*magBin(4)+0.3*magBin(2)
		else
			kp=0.75*magBin(3)+0.25*magBin(2)
		end if
! 		write(6,100) loc(i)%dran,b%magBin%fu,b%magBin%fg,b%magBin%fr,b%magBin%fi,b%magBin%fz,&
! 		b%magBin%d51,b%magBin%kp+(5.0*dlog10(loc(i)%dran))-5.0+(loc(i)%ext*0.88)
! 		
! 		write(6,*) loc(i)%dran,s%mag,b%magBin%kp,loc(i)%ext
			
		!write out app magnitudes from abs magnitudes
		write(6,'(I8,1X,12(E12.5,1X))') id,loc(i)%lran,loc(i)%bran,magBin(:),kp

		DEALLOCATE(magSing,magBin)
	END DO

100 format (8(E12.5,1X))

END PROGRAM makeAllMags