;.reset
file='/padata/beta/users/rfarmer/data/kepler30/channels/WEB_kepler.dat.2'
;file='/padata/beta/users/rfarmer/data/kepler125/channels/WEB_kepler.dat.2'
openr,lun,file,/GET_LUN
nrowsMain=file_lines(file)
dataStruct={num:0.0,m1:0.0,r1:0.0,t1:0.0,l1:0.0,m2:0.0,r2:0.0,t2:0.0,l2:0.0,porb:0.0,birth:0.0,death:0.0,massT:0}
dataMain=replicate(dataStruct,nrowsMain)
readf,lun,dataMain

file='all_weight_eb.2'
;file='WEB_weight.eb.all'
openr,lun,file,/GET_LUN
nrowsWeight=file_lines(file)
dataStructWeight={num:0.0,weight:0.0}
dataWeight=replicate(dataStructWeight,nrowsWeight)
readf,lun,dataWeight

;file='ran_eb.1'
;openr,lun,file,/GET_LUN
;nrowsPop=file_lines(file)
;dataPop=LON64ARR(nrowsPop)
;readf,lun,dataPop

;dataSub=replicate(dataStruct,nrowsPop)
;dataSub=dataMain[dataPop-1]

file='ran_eb_multi.2'
;file='ran_eb_multi.1'
openr,lun,file,/GET_LUN
nrowsErr=file_lines(file)
dataErr=LON64ARR(nrowsErr)
readf,lun,dataErr

;file='kep.r1r2a.D.S.txt'
file='kep.teff.D.S.txt'
;file='kep.period.D.S.txt'
openr,lun,file,/GET_LUN
nrowskepEB=file_lines(file)
kepEB=fltarr(nrowskepEB)
readf,lun,kepEB

file='eb_ran_multi.line.2'
;file='eb_ran_multi.line'
openr,lun,file,/GET_LUN
nrowsLine=file_lines(file)
loop=LON64ARR(nrowsLine)
readf,lun,loop

dataSubErr=replicate(dataStruct,nrowsErr)
dataSubErr=dataMain[dataErr-1]


dataMain2=fltarr(nrowsMain)
;dataSub2=fltarr(nrowsPop)
dataSubErr2=fltarr(nrowsErr)

; dataMain2=(dataMain.m2 < dataMain.m1)/(dataMain.m2 > dataMain.m1)
; dataSub2=(dataSub.m2 < dataSub.m1)/(dataSub.m2 > dataSub.m1)
; dataSubErr2=(dataSubErr.m2 < dataSubErr.m1)/(dataSubErr.m2 > dataSubErr.m1)

dataMain2=(dataMain.t2 < dataMain.t1)/(dataMain.t2 > dataMain.t1)
;dataSub2=(dataSub.t2 < dataSub.t1)/(dataSub.t2 > dataSub.t1)
dataSubErr2=(dataSubErr.t2 < dataSubErr.t1)/(dataSubErr.t2 > dataSubErr.t1)

; gn=6.67384*10.0^(-11)*5.7195044*10.0^(18)
; n = 2.0*3.14/(dataMain.porb/365.0)
; a=(gn*(dataMain.m1+dataMain.m2)/(n^2.0))^(1.0/3.0)
; 
; dataMain2=(dataMain.r1+dataMain.r2)/(1.0*a)
; 
; n = 2.0*3.14/(dataSubErr.porb/365.0)
; a=(gn*(dataSubErr.m1+dataSubErr.m2)/(n^2.0))^(1.0/3.0)
; dataSubErr2=(dataSubErr.r1+dataSubErr.r2)/(1.0*a)

;   dataMain2=alog10(dataMain.porb)
; ; dataSub2=alog10(dataSub.porb)
;   dataSubErr2=alog10(dataSubErr.porb)
;  bad=where(kepEB gt 0.0,count)
;  IF count NE 0 THEN kepEB[bad]=alog10(kepEB[bad])

dataMain=0
dataSubErr=0

numBins=31
;minBin=min(kepEB[WHERE(kepEB gt 0.0)]) < min(dataMain2)
minBin=0.0
maxBin=1.0
binsize=(maxBin-minBin)/(numbins-1.0)
;maxBin=4.5

bad=where(dataMain2 gt maxBin,count)
IF count NE 0 THEN dataMain2[bad]=maxBin

;bad=where(dataSub2 gt maxBin,count)
;IF count NE 0 THEN dataSub2[bad]=maxBin

bad=where(dataSubErr2 gt maxBin,count)
IF count NE 0 THEN dataSubErr2[bad]=maxBin

bad=where(kepEB gt maxBin,count)
IF count NE 0 THEN kepEB[bad]=maxBin


histoMain=histogram(dataMain2,BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64,REVERSE_INDICES=histoMainInd,LOCATIONS=mainHistoLoc)

histoMainTot=fltarr(numBins)
newLoc=fltarr(numBins)

histoMainIndTot=0.0
FOR i=0,numBins-2 DO BEGIN &$
IF histoMainInd[i] NE histoMainInd[i+1] THEN histoMainTot[i]=TOTAL(dataWeight[histoMainInd[histoMainInd[i] : histoMainInd[i+1]-1]].weight) &$
ENDFOR

normHistoMainTot=histoMainTot/(total(histoMainTot)*1.0)

errStd=fltarr(numBins)
avg=fltarr(numBins)
errTotal=LON64ARR(numBins)
errOver=fltarr(numBins)

 errStd[*]=0.0
 minIn=0.0
 count=0.0
 errTotal[*]=0
 FOR j=0,83 DO BEGIN &$
 	minIn=minIn+loop[j]*1000.0 &$
 
 	avgData=(histogram(dataSubErr2[LONG64(minIn):LONG64(minIn+(1000.0*loop[j+1]))-1],BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64))/(1000.0*loop[j+1]) &$
 
 	FOR k=0,999 DO BEGIN &$
 		m1=LONG64(minIn+(k*loop[j+1]*1.0)) &$
 		m2=LONG64(m1*1.0+loop[j+1]*1.0)-1 &$
 		histoErr=histogram(dataSubErr2[m1:m2],BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64,REVERSE_INDICES=errRev) &$
 
 		FOR i=0,numBins-2 DO BEGIN &$
 			IF errRev[i] NE errRev[i+1] THEN count=N_ELEMENTS(dataSubErr2[errRev[errRev[i]:errRev[i+1]-1]]) &$
 
 			IF count gt 1 THEN BEGIN &$
 				errStd[i]=errStd[i]+(count/(loop[j+1]*1.0))^2 &$
 			ENDIF &$
 		ENDFOR &$
 	ENDFOR &$
 	
 	errOver=errOver+(errStd/1000.0)-avgData^2 &$
 	errStd[*]=0.0 &$
 ENDFOR
 
;errStd[WHERE(errTotal gt 0)]=sqrt(errStd[WHERE(errTotal gt 0)]/(1.0*errTotal[WHERE(errTotal gt 0)]))

; histoErr=histogram(dataSubErr2,BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64,REVERSE_INDICES=errRev)
; errMainTot=fltarr(numBins)
; FOR i=0,numBins-2 DO BEGIN &$
; IF errRev[i] NE errRev[i+1] THEN errMainTot[i]=N_ELEMENTS(dataSuberr2[errRev[errRev[i]:errRev[i+1]-1]]) &$
; ENDFOR
; 
; normHistoErrTot=errMainTot/(nrowsErr*1.0)

newLoc[0]=minBin+binSize/2.0
FOR i=1,numBins-1 DO BEGIN &$
newloc[i]=newLoc[i-1]+binSize &$
ENDFOR
;newLoc[numBins-1]=newLoc[numBins-2]

;dataSubHist=histogram(dataSub2,BINSIZE=binsize,MIN=minBin,MAX=maxBin,/L64)

; xaxis='Log!D10!N (P[days])'
; title='Period distribution for Kepler Q0-Q1 EBs and model prediction'

; xaxis='(R!D1!N+R!D2!N)/a'
; title='Distribution of fractional radii for !C Kepler Q0-Q1 EBs and model prediction'

xaxis='T2/T1'
title='T2/T1 distribution for Kepler Q0-Q1 EBs and model prediction'


PS_START,filename='teffRatio.cat1.ps'

cghistoplot,kepEB,BINSIZE=binsize,MININPUT=minBin,MAXINPUT=maxBin,/L64,/FREQUENCY,/WINDOW,MISSING=-1.0,/FILL,xtitle=xaxis,title=title,ymargin=[4,4],MAX_VALUE=MAX(normHistoMainTot+errOver)*1.05

;cghistoplot,kepEB,BINSIZE=binsize,MININPUT=minBin,MAXINPUT=maxBin,/L64,/FREQUENCY,/WINDOW,MISSING=-1.0,/FILL,xtitle=xaxis,title=title,ymargin=[4,4]


;normHistoMainTot[numBins-1]=normHistoMainTot[numBins-2]
cgplot,newloc,normHistoMainTot,/OVERPLOT,/WINDOW

print,normHistoMainTot
bad=where(normHistoMainTot lt 0.0001)
errOver[bad]=0.0

cgplot,newloc,normHistoMainTot+errOver,linestyle=3,/OVERPLOT,/WINDOW
cgplot,newloc,normHistoMainTot-errOver,linestyle=3,/OVERPLOT,/WINDOW

;print,errOver
PS_END,/PNG

END
