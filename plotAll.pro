!EXCEPT=2 
input=COMMAND_LINE_ARGS()
fileMain=input[0]
fileBase=input[1]
fileOut=input[2]


openr,lun,fileMain,/GET_LUN
nrows=file_lines(fileMain)
dataMain=fltarr(nrows)
readf,lun,dataMain
close,lun

numberBins=250
minBin=alog10(MIN(dataMain))
maxBin=alog10(MAX(dataMain))
sizeBin=(maxBin-minBin)/(numberBins-1)


files=file_search(fileBase+'.*')
numFiles=SIZE(files,/N_ELEMENTS)


dataIn=fltarr(nrows/numFiles)

diff=fltarr(numberBins,numFiles)


mainHisto=fltarr(numberBins)
dataHisto=fltarr(numberBins)


mainHisto=histogram(alog10(dataMain),BINSIZE=sizeBin,LOCATION=mass,MIN=minBin,MAX=maxBin,/L64)
mainHisto=mainHisto/(nrows*1.0)

;print,minBin,maxBin,sizeBin
FOR i=0L,numFiles-1 DO BEGIN
	dataHisto=0
	OPENR,lun,files[i],/GET_LUN
	readf,lun,dataIn
	
	dataHisto=histogram(alog10(dataIn),BINSIZE=sizeBin,MIN=minBin,MAX=maxBin,/L64)
	dataHisto=dataHisto/(nrows/numFiles*1.0)

;	print,i,N_ELEMENTS(mainHisto),N_ELEMENTS(dataHisto)	
	diff[*,i]=(mainHisto-dataHisto)
	
	FREE_LUN,lun
ENDFOR

avgArr=fltarr(numberBins)
stddevArr=fltarr(numberBins)
medianArr=fltarr(numberBins)
massAvg=fltarr(N_ELEMENTS(mass))

massAvg=mass+sizeBin/2

FOR i=0,numberBins-1 DO BEGIN
	avgArr[i]=MEAN(diff[i,*])
	stddevArr[i]=stddev(diff[i,*])
	medianArr[i]=median(diff[i,*])
ENDFOR

PS_OPEN,fileOut+'.Avg.ps'
	cghistoplot,alog10(dataMain),BINSIZE=sizeBin,MAX_VALUE=MAX(avgArr+stddevArr+mainHisto)*1.05,MAXINPUT=maxBin,MININPUT=minBin,/FREQUENCY,/L64,xtitle='Log10('+fileOut+')
	cgplot,massAvg,avgArr+mainHisto,/OVERPLOT
	cgplot,massAvg,avgArr+stddevArr+mainHisto,linestyle=3,/OVERPLOT
	cgplot,massAvg,avgArr-stddevArr+mainHisto,linestyle=3,/OVERPLOT
PS_CLOSE

PS_OPEN,fileOut+'.File5806.ps'
	OPENR,lun,files[5805],/GET_LUN
	readf,lun,dataIn

	cghistoplot,alog10(dataIn),BINSIZE=sizeBin,MAX_VALUE=MAX(avgArr+stddevArr+mainHisto)*1.05,/FREQUENCY,MAXINPUT=maxBin,MININPUT=minBin,/L64,xtitle='Log10('+fileOut+')
	cgplot,massAvg,avgArr+mainHisto,/OVERPLOT
	cgplot,massAvg,avgArr+stddevArr+mainHisto,linestyle=3,/OVERPLOT
	cgplot,massAvg,avgArr-stddevArr+mainHisto,linestyle=3,/OVERPLOT
	
	FREE_LUN,lun
PS_CLOSE


END
