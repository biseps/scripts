! Removes entries we dont want from the kic dataset
! Galaxies
! Out of seasons 1 ccd area
! mag 8-16
! reapeated stars?

program checkKic
	IMPLICIT NONE
	INTEGER :: j,k,aerr,reason
	INTEGER :: i,lineCount1
	INTEGER,PARAMETER :: lineCount2=336
	
	LOGICAL :: fileExists,pnpoly
      CHARACTER(len=1000) :: fin,fin2
      CHARACTER(len=1000) :: BUFFER

	DOUBLE PRECISION,DIMENSION(1:lineCount2) :: long,lat
	DOUBLE PRECISION,PARAMETER :: EPS=1.d-8
	DOUBLE PRECISION,DIMENSION(1:5) :: tlong,tlat
	DOUBLE PRECISION, PARAMETER :: G=6.67384d-11
	DOUBLE PRECISION, PARAMETER :: Msun=1.98892d30,Rsun=6.955d8


! 	TYPE kic
! 		DOUBLE PRECISION :: ra,dec,pmra,pmdec
! 		DOUBLE PRECISION :: umag,gmag,rmag,imag,zmag,gred,d51,jmag,hmag,kmag,kepmag
! 		INTEGER :: id,tmid,scpid,altid,altsource,galaxy,blend,variable
! 		DOUBLE PRECISION :: teff,logg,feh,eb,av,rad
! 		CHARACTER(len=5) :: cq
! 		INTEGER :: pq,aq,catkey,scpkey
! 		DOUBLE PRECISION :: parra,glon,glat,pmtotal,gr,jk,gk,degra
! 		INTEGER :: fov
! 		CHARACTER(len=100) :: tm
! 	END TYPE kic
 	TYPE kic
		DOUBLE PRECISION :: umag,gmag,rmag,imag,zmag,gred,d51,jmag,hmag,kmag,kepmag
		DOUBLE PRECISION :: glon,glat
	END TYPE kic


	TYPE(kic), DIMENSION(:),ALLOCATABLE :: s


! 	s%long=0.d0
! 	s%lat=0.d0
! 	s%mag=0.d0

	!KIC.reduce3.txt
	call GETARG(1,fin)
	!CCD coordinates in long lat
	call GETARG(2,fin2)

	fin=fin(1:LEN_TRIM(fin))
	fin2=fin2(1:LEN_TRIM(fin2))
	
	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
 	
 	fileExists=.false.
 	inquire(FILE=fin2,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if

	lineCount1=0
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
	DO WHILE (.true.)
		read(1, '(A)', end=99) BUFFER
		lineCount1=lineCount1+1
	ENDDO
99  	CONTINUE
	CLOSE(1)

	aerr=0
!  	ALLOCATE(s(1:lineCount1),STAT=aerr)
! 	IF (aerr > 0) THEN
! 		WRITE(0,*)
! 		WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
! 		WRITE(0,*)
! 		STOP 'Abnormal program termination!!!'
! 	END IF
 	ALLOCATE(s(1:lineCount1))
! 	IF (aerr > 0) THEN
! 		WRITE(0,*)
! 		WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
! 		WRITE(0,*)
! 		STOP 'Abnormal program termination!!!'
! 	END IF


	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount1
		READ(1,*) s(i)%glon,s(i)%glat,s(i)%umag,s(i)%gmag,s(i)%rmag,&
		s(i)%imag,s(i)%zmag,s(i)%gred,s(i)%d51,s(i)%jmag,s(i)%hmag,&
		s(i)%kmag,s(i)%kepmag
	end do
	CLOSE(1)


	j=0
	k=1
	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
	DO i=1,84
		tlong=0.d0
		tlat=0.d0
		READ(1,*)tlong(1),tlat(1)
		READ(1,*)tlong(2),tlat(2)
		READ(1,*)tlong(3),tlat(3)
		READ(1,*)tlong(4),tlat(4)
		READ(1,*)tlong(5),tlat(5)
		
		long(k)=MINVAL(tlong)
! 		j=MINLOC(tlong)
		lat(k)=tlat(MINLOC(tlong,1))

		long(k+1)=tlong(MINLOC(tlat,1))
		lat(k+1)=MINVAL(tlat)

		long(k+2)=MAXVAL(tlong)
		lat(k+2)=tlat(MAXLOC(tlong,1))

		long(k+3)=tlong(MAXLOC(tlat,1))
		lat(k+3)=MAXVAL(tlat)

		k=k+4
	END DO
	CLOSE(1)

	outer: DO i=1,lineCount1
		inner: DO j=1,84
			if(pnpoly(s(i)%glon,s(i)%glat,long(1+((j-1)*4):j*4),lat(1+((j-1)*4):j*4)))THEN
				IF (s(i)%kepmag .ge.8.0 .and. s(i)%kepmag .le. 16.0) THEN
! 					if(s(i)%logg < 999999.9 .and. s(i)%rad < 999999.9) THEN
						write(6,*) "1.0 ",s(i)%umag,s(i)%gmag,s(i)%rmag,&
						s(i)%imag,s(i)%zmag,s(i)%gred,s(i)%d51,s(i)%jmag,s(i)%hmag,&
						s(i)%kmag,s(i)%kepmag
! 					else
! 						write(6,*) s(i)%glon,s(i)%glat,s(i)%umag,s(i)%gmag,s(i)%rmag,&
! 						s(i)%imag,s(i)%zmag,s(i)%gred,s(i)%d51,s(i)%jmag,s(i)%hmag,&
! 						s(i)%kmag,s(i)%kepmag
! 					end if
				end if
				cycle outer
			end if
		end do inner
	end do outer
	

end program checkKic

	PURE LOGICAL FUNCTION PNPOLY(x,y,px,py)
	IMPLICIT NONE
	INTEGER :: i,j
	INTEGER,PARAMETER :: N=4
	DOUBLE PRECISION,INTENT(IN) :: x,y
	DOUBLE PRECISION,INTENT(IN),DIMENSION(0:N-1) :: px,py

	PNPOLY=.FALSE.

	DO i=0,n-1
		j=MOD(i+1,N)
		IF((py(i).gt.y).neqv. (py(j).gt.y) )THEN
			IF(x.lt.(px(j)-px(i))*(y-py(i))/(py(j)-py(i))+px(i)) THEN
				PNPOLY=.NOT.PNPOLY
			END IF
		END IF
	END DO

	END FUNCTION PNPOLY