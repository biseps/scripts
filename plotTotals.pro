.reset
file='eb_count'
fileout='eb_count.ps'

nrows=file_lines(file)
num=fltarr(nrows)
openr,lun,file,/GET_LUN
readf,lun,num

num=num*0.5

file='/padata/beta/users/rfarmer/code/scripts/keplerCCD2.txt'
nrows2=file_lines(file)
openr,lun,file,/GET_LUN

dataIn=fltarr(2,nrows2)
ra=fltarr(nrows2)
dec=fltarr(nrows2)
gl=fltarr(nrows2)
gb=fltarr(nrows2)

readf,lun,dataIn


ra=dataIn(0,*)
dec=dataIn(1,*)

glactc,ra,dec,2000,gl,gb,1,/DEGREE

  xlab = 'Galactic longitude, l (degrees)'
  ylab = 'Galactic latitude, b (degrees)'

; Leave some place at the top for a color bar legend

  plot_position = [0.14,0.13,0.94,0.84]
  bar_position  = [0.14,0.86,0.94,0.91]

fmin=0.0
fmax=MAX(num)

; Load color table
rbasic = [255,202,150,180,177,175,112, 50,  0]
gbasic = [255,237,220,115, 57,  0,  0,  0,  0]
bbasic = [100, 85, 70,  0,  0,  0,127,255,100]

  nbasic = SIZE(rbasic, /N_ELEMENTS)
;  nsub = 200
  nsub = 30
  ntotal = (nsub-1)*(nbasic-1)+1

  r = INTARR(ntotal)
  g = INTARR(ntotal)
  b = INTARR(ntotal)

  FOR i = 1, nbasic-1 DO BEGIN &$

     r1 = FLOAT(rbasic[i-1]) &$
     r2 = FLOAT(rbasic[i]) &$
     dr = (r2-r1)/(nsub-1) &$
     rsub = FIX(r1+FINDGEN(nsub)*dr) &$
     FOR j = 0, nsub-2 DO r[(i-1)*(nsub-1)+j] = rsub[j] &$

     g1 = FLOAT(gbasic[i-1]) &$
     g2 = FLOAT(gbasic[i]) &$
     dg = (g2-g1)/(nsub-1) &$
     gsub = FIX(g1+FINDGEN(nsub)*dg) &$
     FOR j = 0, nsub-2 DO g[(i-1)*(nsub-1)+j] = gsub[j] &$

     b1 = FLOAT(bbasic[i-1]) &$
     b2 = FLOAT(bbasic[i]) &$
     db = (b2-b1)/(nsub-1) &$
     bsub = FIX(b1+FINDGEN(nsub)*db) &$
     FOR j = 0, nsub-2 DO b[(i-1)*(nsub-1)+j] = bsub[j] &$

  ENDFOR

  r[ntotal-1] = rbasic[nbasic-1]
  g[ntotal-1] = gbasic[nbasic-1]
  b[ntotal-1] = bbasic[nbasic-1]

  ncol = ntotal

  red   = [0,255,r]
  green = [0,255,g]
  blue  = [0,255,b]

  TVLCT, [red], [green], [blue]

; Set up color coding bins

  df = (fmax-fmin)/FLOAT(ncol)
  fbin = FINDGEN(ncol+1)*df+fmin

; Set up color coding bins

  df = (fmax-fmin)/FLOAT(ncol)
  fbin = FINDGEN(ncol+1)*df+fmin

PS_START,filename=fileOut

 
cgplot,[0],[0],xrange=[min(gl),max(gl)],yrange=[min(gb),max(gb)], BACKGROUND=1, COLOR=0, POSITION=plot_position,xtitle=xlab,ytitle=ylab,/NODATA
	
i=0
xt=fltarr(5)
yt=fltarr(5)
for j=0,(nrows2/5)-1 do begin &$

  	k = 0L &$
      REPEAT BEGIN &$
    	  k = k + 1 &$
        IF (k GT ncol) THEN BREAK  &$
      ENDREP UNTIL ((num[j] GT fbin[k-1]) AND (num[j] LE fbin[k])) &$
      col = k-1 &$
	col = col + 2 &$

	i=j*5 &$
	cgplots,gl[i+1],gb[i+1],/DATA,color=0 &$
	cgplots,gl[i],gb[i],/CONTINUE,/DATA,color=0 &$
	cgplots,gl[i+3],gb[i+3],/CONTINUE,/DATA,color=0 &$
	cgplots,gl[i+4],gb[i+4],/CONTINUE,/DATA,color=0 &$
	cgplots,gl[i+1],gb[i+1],/CONTINUE,/DATA,color=0 &$

	xt[0]=gl[i+1] &$
	xt[1]=gl[i] &$
	xt[2]=gl[i+3] &$
	xt[3]=gl[i+4] &$
	xt[4]=gl[i+1] &$
	
	yt[0]=gb[i+1] &$
	yt[1]=gb[i] &$
	yt[2]=gb[i+3] &$
	yt[3]=gb[i+4] &$
	yt[4]=gb[i+1] &$
	
	polyfill,xt,yt,COLOR=col,/DATA &$
ENDFOR

titleSingle='Single star count'
titleBinary='Binary star count, p <= 125 days'
titleEB='EB star count, p <= 125 days,'+greek('Delta')+'F/F > 10!E-4'

cgCOLORBAR, RANGE=[fmin,fmax], BOTTOM=2, COLOR=0, NCOLORS=ncol, $
       POSITION=bar_position, FORMAT='(E11.4)', CHARSIZE=1.1,   $
       DIVISIONS=5, /TOP,title=titleEB

al_legend,['Total ='+strtrim(STRING(LONG((TOTAL(num)))),2)], /right_legend,/top_legend

PS_END,/PNG
EXIT

