MODULE helpers
	CONTAINS
      ELEMENTAL DOUBLE PRECISION FUNCTION rlobe(a,m1,m2)

! Function to evaluate the radius of the Roche lobe
! Bart Willems, 17/11/00

! m1 and m2 are expressed in solar masses
! a and rlobe are expressed in solar radii

      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: a,m1,m2
      DOUBLE PRECISION :: q13,fq

      q13 = (m1/m2)**(1.d0/3.d0)
      fq = 0.49d0*q13*q13/(0.6d0*q13*q13+LOG(1.d0+q13))
      rlobe = a*fq

      RETURN
      END FUNCTION rlobe


      ELEMENTAL DOUBLE PRECISION FUNCTION Porb2a(Porb,m1,m2)

! Function to evaluate the semi-major axis from Kepler's 3rd law
! Bart Willems, 30/11/00

! Porb is expressed in years
! m1 and m2 are expressed in solar masses
! a is expressed in solar radii
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: Porb,m1,m2
      DOUBLE PRECISION :: n

      n = 2.d0*pi/Porb
      Porb2a = (gn*(m1+m2)/n**2)**(1.d0/3.d0)

      RETURN
      END FUNCTION Porb2a

END MODULE helpers


PROGRAM getBinaryType
! Works out whether a binary is detached,semi,overcontact,ellisopdal(?)
! also the stars fillout factor
	USE dtypes
	USE helpers
IMPLICIT NONE
	INTEGER :: i,j,k,lineCount,aerr,tmpInt,clock
	TYPE(stq),ALLOCATABLE,DIMENSION(:,:):: s
	DOUBLE PRECISION,ALLOCATABLE,DIMENSION(:,:) :: roche
	TYPE(bnq),ALLOCATABLE,DIMENSION(:) :: b
	DOUBLE PRECISION ::tmp,num,ran,EPSover=0.01,EPSellip=0.1
! 	DOUBLE PRECISION :: Porb2a,rlobe
	CHARACTER(len=1),ALLOCATABLE,DIMENSION(:) :: rocheType
	CHARACTER(len=100) :: fin,fin2
	CHARACTER(len=1000) :: BUFFER
	CHARACTER(len=10) :: inTrans	
	LOGICAL :: fileExists

	!WEB_kepler.dat.1
	call GETARG(1,fin)

	fin=fin(1:len_trim(fin))

!Read in WEB_kepler file and generate transit prob for each line tht transit is > kepler Min 10^-4
	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if


 	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
	DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  		lineCount=lineCount+1
      ENDDO
99    CONTINUE
      CLOSE(1)

	aerr=0
      ALLOCATE(s(1:2,1:lineCount),b(1:lineCount),roche(1:2,1:lineCount),&
      rocheType(1:lineCount),STAT=aerr)
	IF (aerr > 0) THEN
		WRITE(0,*)
		WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
		WRITE(0,*)
		STOP 'Abnormal program termination!!!'
	END IF


 	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
      DO i=1,100
		READ(1,'(F12.4,1X,11(E18.12,1X),I3,A)') &
			num,s(1,i)%mt,s(1,i)%reff,s(1,i)%teff,s(1,i)%lum,&
			s(2,i)%mt,s(2,i)%reff,s(2,i)%teff,s(2,i)%lum,&
			b(i)%porb,tmp,tmp,tmpInt,BUFFER	
      END DO
	CLOSE(1)
	b%a=Porb2a(b%porb/365.0,s(1,:)%mt,s(2,:)%mt)

	roche(1,:)=rlobe(b%a,s(1,:)%mt,s(2,:)%mt)
	roche(2,:)=rlobe(b%a,s(2,:)%mt,s(1,:)%mt)

	DO i=1,100
		if(abs(roche(1,i)-s(1,i)%reff)/s(1,i)%reff .lt. EPSover .and. &
		abs(roche(2,i)-s(2,i)%reff)/s(2,i)%reff .lt. EPSover) THEN
			rocheType(i)='o'
			cycle
		else if (abs(roche(1,i)-s(1,i)%reff)/s(1,i)%reff .lt. EPSover .or. &
			   abs(roche(2,i)-s(2,i)%reff)/s(2,i)%reff .lt. EPSover) THEN
			rocheType(i)='s'
			cycle
		else if (abs(roche(1,i)-s(1,i)%reff)/s(1,i)%reff .lt. EPSellip .or. &
			   abs(roche(2,i)-s(2,i)%reff)/s(2,i)%reff .lt. EPSellip) THEN
			rocheType(i)='e'
			cycle
		else
			rocheType(i)='d'
			cycle
		end if
	END DO

	do i=1,100
	if(rocheType(i)=='o')then
	write(*,*) roche(1,i),s(1,i)%reff,abs(roche(1,i)-s(1,i)%reff)/s(1,i)%reff
	write(*,*) roche(2,i),s(2,i)%reff,abs(roche(2,i)-s(2,i)%reff)/s(2,i)%reff
	write(*,*) rocheType(i)
	write(*,*)
	end if
	end do
	
END PROGRAM getBinaryType
