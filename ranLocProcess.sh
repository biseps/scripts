#!/bin/bash
#$ -N popProcess
#$ -P physics
#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=4G
#$ -S /bin/bash
#$ -t 1-84:1
#$ -j y
#$ -o /padata/beta/users/rfarmer/data/keplerAll/cluster2

id=$1

script="/padata/beta/users/rfarmer/code/scripts"
source="/padata/beta/users/rfarmer/code/source"

base="/padata/beta/users/rfarmer/data/keplerAll"
version=1
type="WEB"

young="$base/pop_b_y/$SGE_TASK_ID"
old="$base/pop_b_o/$SGE_TASK_ID"
merge="$base/merge/b/$SGE_TASK_ID"

# for version in $(seq 1 12)
# do

cd $young
echo $young $version
# # $script/weightTrans "$type"_popCount ../../merge/b/EB.txt > "$type"_weight.$version
# # $script/ranPick "$type"_weight.$version > "$type"_ran.$version
$script/ranPick "$type"_popCount 1.0 > "$type"_ran.$version
$script/ranLoc "$type"_ran.$version ../../channels/"$type"_mag.dat.1 pop.in > "$type"_ranLoc.$version

cd $old
echo $old $version
# $script/weightTrans "$type"_popCount ../../merge/b/EB.txt > "$type"_weight.$version
# $script/ranPick "$type"_weight.$version > "$type"_ran.$version
$script/ranPick "$type"_popCount 1.0 > "$type"_ran.$version
$script/ranLoc "$type"_ran.$version ../../channels/"$type"_mag.dat.1 pop.in > "$type"_ranLoc.$version

echo "cat files"
cat "$young"/"$type"_ranLoc.$version "$old"/"$type"_ranLoc.$version > $merge/"$type"_ranLoc.$version
# done



