#!/bin/bash
#$ -N popProcess
#$ -P physics
#$ -l s_rt=100:00:00,virtual_free=3G,h_vmem=4G
#$ -t 1-84:1
#$ -S /bin/bash
#$ -j y
#$ -o /padata/beta/users/rfarmer/data/kepler125/merge/b/cluster

script="/padata/beta/users/rfarmer/data/scripts"

base="/padata/beta/users/rfarmer/data/kepler125"
version=1

cd $base/merge/b/$SGE_TASK_ID


 $script/merge ../../../pop_b_y/$SGE_TASK_ID/WEB_popCount ../../../pop_b_o/$SGE_TASK_ID/WEB_popCount > WEB_merge.$version
 awk '{var+=$2}END{print var}' WEB_merge.$version > merge.count.$version
 $script/weightTrans WEB_merge.$version ../EB.txt > WEB_weight.$version
 awk '{var+=$2}END{print var}' WEB_weight.$version > weight.count.$version
 $script/ranPick WEB_merge.$version > WEB_ran_bin.$version
 $script/ranPick WEB_weight.$version > WEB_ran.eb.$version
 $script/ranPickMulti WEB_weight.$version > WEB_ran_eb_multi.$version
# $script/chanProc $base/channels/WEB_pop_dat.1 WEB_merge.$version $base/channels/WEB.chd.1 > bin_type.$version
# $script/chanProc2 eb_type.$version > eb_type_sum.$version
