#!/bin/bash


makefile="./Makefile_ranLoc"
extra_folder="../pop"


# remove executable 
printf "\n\nremoving existing 'ranLoc'...\n"
make -f $makefile folder=$extra_folder clobber


# compile using make
printf "\n\ncompiling 'ranLoc'...\n"
make -f $makefile folder=$extra_folder

