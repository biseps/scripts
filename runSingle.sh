#!/bin/bash
{
gfortran -o ranPick -O2 numrec.f90 ranPick.f90
rm *.mod *.o 2> /dev/null

./ranPick singlesPop.txt WEBwide_kepler.dat.1 single/draw/draw

#binary
for((i=1;i<=10000;i++)); do awk -v num="$i" '{print $2 > "mass1/mass1."num; print $3 > "radius1/radius1."num;print $4 > "teff1/teff1."num;print $5 > "mass2/mass2."num; print $6 > "radius2/radius2."num;print $7 > "teff2/teff2."num;print $8 > "period/period."num; if($2>$5) {print $5/$2 > "massR/massR."num} else {print $2/$5 > "massR/massR."num} }' draw/draw.$i;done

for((i=1;i<=10000;i++)); do awk -v num="$i" '{print $2 > "mass/mass."num; print $3 > "radius/radius."num;print $4 > "teff/teff."num;}' draw/draw.$i;done


idl -e '.r plotAll.pro' -args mass single single
idl -e '.r plotAll.pro' -args radi single single
idl -e '.r plotAll.pro' -args teff single single

exit 0
}
