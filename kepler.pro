.reset
file='allEB.txt'
fileOut=file+'.ps'

dataStruct={long:0.0,lat:0.0,num:0.0}
nrows=file_lines(file)
data=replicate(dataStruct,nrows)
openr,lun,file,/GET_LUN
readf,lun,data

  xlab = 'Galactic longitude, l (degrees)'
  ylab = 'Galactic latitude, b (degrees)'

; Leave some place at the top for a color bar legend

  plot_position = [0.14,0.13,0.94,0.84]
  bar_position  = [0.14,0.86,0.94,0.91]

x=fltarr(nrows)
y=fltarr(nrows)
diff=5.0^0.5

xs = data.long
ys = data.lat
w = diff/2
xbox = [1,1,-1,-1,1]*w
ybox = [-1,1,1,-1,-1]*w

fmin=0.0
fmax=MAX(data.num)

; Load color table
rbasic = [255,202,150,180,177,175,112, 50,  0]
gbasic = [255,237,220,115, 57,  0,  0,  0,  0]
bbasic = [100, 85, 70,  0,  0,  0,127,255,100]

  nbasic = SIZE(rbasic, /N_ELEMENTS)
;  nsub = 200
  nsub = 30
  ntotal = (nsub-1)*(nbasic-1)+1

  r = INTARR(ntotal)
  g = INTARR(ntotal)
  b = INTARR(ntotal)

  FOR i = 1, nbasic-1 DO BEGIN &$

     r1 = FLOAT(rbasic[i-1]) &$
     r2 = FLOAT(rbasic[i]) &$
     dr = (r2-r1)/(nsub-1) &$
     rsub = FIX(r1+FINDGEN(nsub)*dr) &$
     FOR j = 0, nsub-2 DO r[(i-1)*(nsub-1)+j] = rsub[j] &$

     g1 = FLOAT(gbasic[i-1]) &$
     g2 = FLOAT(gbasic[i]) &$
     dg = (g2-g1)/(nsub-1) &$
     gsub = FIX(g1+FINDGEN(nsub)*dg) &$
     FOR j = 0, nsub-2 DO g[(i-1)*(nsub-1)+j] = gsub[j] &$

     b1 = FLOAT(bbasic[i-1]) &$
     b2 = FLOAT(bbasic[i]) &$
     db = (b2-b1)/(nsub-1) &$
     bsub = FIX(b1+FINDGEN(nsub)*db) &$
     FOR j = 0, nsub-2 DO b[(i-1)*(nsub-1)+j] = bsub[j] &$

  ENDFOR

  r[ntotal-1] = rbasic[nbasic-1]
  g[ntotal-1] = gbasic[nbasic-1]
  b[ntotal-1] = bbasic[nbasic-1]

  ncol = ntotal

  red   = [0,255,r]
  green = [0,255,g]
  blue  = [0,255,b]

  TVLCT, [red], [green], [blue]

; Set up color coding bins

  df = (fmax-fmin)/FLOAT(ncol)
  fbin = FINDGEN(ncol+1)*df+fmin

; Set up color coding bins

  df = (fmax-fmin)/FLOAT(ncol)
  fbin = FINDGEN(ncol+1)*df+fmin

;PS_START,filename=fileOut
 
cgplot,data.long,data.lat,psym=4,xrange=[min(data.long-5),max(data.long+5)],yrange=[min(data.lat-5),max(data.lat+5)], BACKGROUND=1, COLOR=0, POSITION=plot_position,xtitle=xlab,ytitle=ylab

for i = 0l, nrows-1 do begin &$

  	j = 0L &$
    REPEAT BEGIN &$
    	j = j + 1 &$
        IF (j GT ncol) THEN BREAK  &$   
    ENDREP UNTIL ((data[i].num GT fbin[j-1]) AND (data[i].num LE fbin[j])) &$
    col = j-1 &$
	col = col + 2 &$
    xt = xs[i] + xbox   &$
    yt = ys[i] + ybox   &$

polyfill,xt,yt,COLOR=col &$
cgplots, xt, yt, /DATA,thick=1,COLOR=0  &$

;xyouts,data[i].long-3,data[i].lat,i+1,/DATA,COLOR=255 &$
ENDFOR

XYOUTS, 255.0, 110.0, 'N', COLOR=0, ORIENTATION=90

cgCOLORBAR, RANGE=[0,MAX(data.num)], BOTTOM=2, COLOR=0, NCOLORS=ncol, $
       POSITION=bar_position, FORMAT='(F6.1)', CHARSIZE=1.1,   $
       DIVISIONS=5, /TOP,title='Number of EBs with secondary transit > 10!E-!E4'
       
al_legend,['Total ='+strtrim(STRING((LONG(TOTAL(data.num)))),2)], /right_legend,/top_legend
      

file='/padata/beta/users/rfarmer/data/scripts/keplerCCD2.txt'
nrows=file_lines(file)
openr,lun,file,/GET_LUN

dataIn=fltarr(2,nrows)
ra=fltarr(nrows)
dec=fltarr(nrows)
gl=fltarr(nrows)
gb=fltarr(nrows)

readf,lun,dataIn


ra=dataIn(0,*)
dec=dataIn(1,*)

glactc,ra,dec,2000,gl,gb,1,/DEGREE
i=0
for j=0,(nrows/5)-1 do begin &$
	i=j*5 &$
	cgplots,gl[i+1],gb[i+1],/DATA,color=0 &$
	cgplots,gl[i],gb[i],/CONTINUE,/DATA,color=0 &$
	cgplots,gl[i+3],gb[i+3],/CONTINUE,/DATA,color=0 &$	
	cgplots,gl[i+4],gb[i+4],/CONTINUE,/DATA,color=0 &$	
	cgplots,gl[i+1],gb[i+1],/CONTINUE,/DATA,color=0 &$	
ENDFOR 

;PS_END,/PNG
