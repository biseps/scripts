#!/bin/bash

string='
<Placemark>
<name>Our Field </name>
<styleUrl>#msn_ylw-pushpin</styleUrl>
−
<Polygon>
<tessellate>0</tessellate>
−
<outerBoundaryIs>
−
<LinearRing>
−
<coordinates>

				          $x1,$y1
				          $x2,$y2
				          $x3,$y3
				          $x4,$y4
				          $x1,$y1
                                
</coordinates>
</LinearRing>
</outerBoundaryIs>
</Polygon>
</Placemark>
'