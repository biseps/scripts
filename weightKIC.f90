PROGRAM weightTrans
	
	IMPLICIT NONE

	INTEGER :: i,j,k,lineCount,lineCount2,aerr
	
	DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:) :: num,prob
	DOUBLE PRECISION, ALLOCATABLE,DIMENSION(:,:) :: kic
	INTEGER, ALLOCATABLE,DIMENSION(:,:) :: kic2

	LOGICAL :: fileExists
	CHARACTER(len=100) :: fin,fin2
	CHARACTER(len=1000) :: BUFFER

	!WEB_merge
	call GETARG(1,fin)
	!kic.1
	call GETARG(2,fin2)

	fileExists=.false.
	inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
	if(fileExists .eqv. .false.) then
		write(0,*) "No file ", fin
		stop
	end if

	fileExists=.false.
	inquire(FILE=fin2(1:LEN_TRIM(fin2)),exist=fileExists)
	if(fileExists .eqv. .false.) then
		write(0,*) "No file ", fin2
		stop
	end if
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
	DO WHILE (.true.)
		read(1, '(A)', end=99) BUFFER
		lineCount=lineCount+1
	ENDDO
99  	CONTINUE
	CLOSE(1)

	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount2=0
	DO WHILE (.true.)
		read(1, '(A)', end=98) BUFFER
		lineCount2=lineCount2+1
	ENDDO
98  	CONTINUE
	CLOSE(1)


	if(lineCount.ne.lineCount2)THEN
		WRITE(0,*)'1, Lines dont match!'
		WRITE(0,*)fin, fin2
		WRITE(0,*)lineCount,lineCount2
		STOP 'Abnormal program termination!!!'
	END IF


	
	aerr=0
	ALLOCATE(num(1:lineCount),prob(1:lineCount),kic(1:13,1:lineCount),kic2(1:13,1:lineCount))
	IF (aerr > 0) THEN
		WRITE(0,*)
		WRITE(0,*) '1, ERROR ALLOCATING MEMORY!'
		WRITE(0,*)
		STOP 'Abnormal program termination!!!'
	END IF
	
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
	READ(1,*) (num(i),prob(i), i=1,lineCount)
	CLOSE(1)

	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount
		READ(1,*) (kic(j,i), j=1,13)
	END DO
	CLOSE(1)

	DO i=1,lineCount
		do j=1,13
			kic(j,i)=kic(j,i)*prob(i)
		ENDDO
	END DO
	
	DO i=1,13	
		write(*,*) SUM(kic(i,:))
	END DO
	
END PROGRAM weightTrans