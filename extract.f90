PROGRAM extractLines
	IMPLICIT NONE
	INTEGER(kind=8) :: i,j,lineCount1,lineCount2,aerr=0
	
	LOGICAL :: fileExists
	CHARACTER(len=1000) :: fin,fin2
	CHARACTER(len=1000) :: BUFFER
	
	INTEGER(kind=8),ALLOCATABLE,DIMENSION(:) :: inputLines
	CHARACTER(len=1000) :: line

	!File with line nunmbers
	call GETARG(1,fin)
	!Any file
	call GETARG(2,fin2)

	fin=fin(1:LEN_TRIM(fin))
	fin2=fin2(1:LEN_TRIM(fin2))
	
	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
 	
 	fileExists=.false.
 	inquire(FILE=fin2,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if
 	
 	
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount1=0
	DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  		lineCount1=lineCount1+1
      ENDDO
99    CONTINUE
      CLOSE(1)

      OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')

	lineCount2=0
	DO WHILE (.true.)
  		read(1, '(A)', end=98) BUFFER
  		lineCount2=lineCount2+1
      ENDDO
 98    CONTINUE
      CLOSE(1)
!     
!       ALLOCATE(inputLines(1:lineCount1),line(1:lineCount2),STAT=aerr)
!          IF (aerr > 0) THEN
!             WRITE(*,*)
!             WRITE(*,*) '1, ERROR ALLOCATING MEMORY!'
!             WRITE(*,*)
!             STOP 'Abnormal program termination!!!'
!          END IF

	ALLOCATE(inputLines(1:lineCount1),STAT=aerr)
         IF (aerr > 0) THEN
            WRITE(*,*)
            WRITE(*,*) '1, ERROR ALLOCATING MEMORY!'
            WRITE(*,*)
            STOP 'Abnormal program termination!!!'
         END IF


	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
	DO i=1,lineCount1
		READ(1,*) inputLines(i)
	END DO
	CLOSE(1)

 	OPEN(1,FILE=fin2,STATUS='OLD',ACTION='READ')
! 	DO i=1,lineCount2
! 		READ(1,'(A)') line(i)
! 	END DO
! 	CLOSE(1)
! 
! 	DO i=1,lineCount1
! 		WRITE(*,*) inputLines(i),trim(line(inputLines(i)))
! 	END DO

	!Read fin 2 in only as needed this way we dont run out of memoray
	j=1
	DO i=1,lineCount2
		READ(1,'(A)') line
		DO WHILE (i==inputLines(j).and. j.le.lineCount1) 
			WRITE(*,'(I10,1X,A)') inputLines(j),trim(line)
			j=j+1
		END DO
		IF(j.gt.lineCount1) EXIT
	END DO
	CLOSE(1)

END PROGRAM extractLines