PROGRAM makeMags
! Given stellar data will make a file to be read into IDL for the KIC 
	USE dtypes
	USE magnitude
	USE params
	USE bfuncs
IMPLICIT NONE
	INTEGER :: i,j,k,lineCount,aerr,tmpInt,id
	TYPE(stq),DIMENSION(1:2):: s
	TYPE(bnq) :: b
	DOUBLE PRECISION ::tmp,num,ran
	CHARACTER(len=100) :: fin,fin2
	CHARACTER(len=1000) :: BUFFER,BUFFER2
	CHARACTER(len=10) :: inTrans
	CHARACTER(LEN=1) :: in
	LOGICAL :: fileExists


	filterBand='Kp'
	isSingle=0
	!_kepler.dat.1 thats been thropugh extract
	call GETARG(1,fin)
	!is Single yes =1 no =0
	call GETARG(2,in)
	read(in,*) isSingle

	fin=fin(1:len_trim(fin))
		
	CALL preIntMag

	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if

 	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
	DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  		lineCount=lineCount+1
      ENDDO
99    CONTINUE
      CLOSE(1)


 	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
      DO i=1,lineCount
		READ(1,*) id,num,s(1)%mt,s(1)%reff,s(1)%teff,s(1)%lum,&
			s(2)%mt,s(2)%reff,s(2)%teff,s(2)%lum,&
			b%porb,tmp,tmp,tmpInt,BUFFER

		CALL getMagBC(s,b)

! 		write(*,*) i,'0.0 0.0 0.0 0.0 ',&
! 		b%magBin%fu,'0.001 ',b%magBin%fg,'0.001 ',b%magBin%fr,'0.001 ',b%magBin%fi,'0.001 ',b%magBin%fz,'0.001 ',&
! 		b%magBin%d51,'0.001 0.0 10.0 ',&
! 		b%magBin%jj,b%magBin%jh,b%magBin%jk,&
! 		'0 0 0 0.0 0 0'
		write(*,*) id,b%magBin%fu,b%magBin%fg,b%magBin%fr,b%magBin%fi,&
		b%magBin%fz,b%magBin%d51,b%magBin%jj,b%magBin%jh,b%magBin%jk,b%magBin%kp

	END DO	
	CLOSE(1)

END PROGRAM makeMags