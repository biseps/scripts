PROGRAM getType
	IMPLICIT NONE
!Given a final state of a star find all that conatin this type
	INTEGER :: i,j,k,lineCount

 	CHARACTER(len=4) :: dataLine
	CHARACTER(len=1000) :: line
	DOUBLE PRECISION :: tmp

	LOGICAL :: fileExists
      CHARACTER(len=100) :: fin,fin2
      CHARACTER(len=2) :: final
      CHARACTER(len=1000) :: BUFFER

	!file to extract from
	call GETARG(1,fin)
	!Type of star
	call GETARG(2,final)

	fileExists=.false.
 	inquire(FILE=fin(1:LEN_TRIM(fin)),exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	lineCount=0
      DO WHILE (.true.)
		read(1, '(A)', end=99) BUFFER
		lineCount=lineCount+1
  	ENDDO
99  	CONTINUE
    	CLOSE(1)

    	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	DO i=1,lineCount
	    	READ(1,*) tmp,tmp,tmp,tmp,tmp,tmp,line
		dataLine=line(len_trim(line)-3:len_trim(line))
		IF(dataLine(1:2)==final .or. dataLine(3:4)==final) write(*,*) i
	END DO
	CLOSE(1)

END PROGRAM